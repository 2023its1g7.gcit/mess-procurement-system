<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GCIT Mess</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user.css') }}">

</head>

<body>
    @include('sidebar')
    <br>

    <div class="container-fluid">
        <div class="row content">
            <div class="col-sm-9">
                <div class="topbar">
                    <h4>Edit Issued List</h4>
                </div>

                <div class="col-sm-12">
                  <div style="border-radius: 10px; border: 1px solid rgba(0, 0, 0, 0.30); background: #FFF; margin-top:20px; height: auto; flex-shrink: 0; padding:30px;" >
                    
                  <form method="POST" action="{{ route('updateIssue', ['issue_id' => $issue->issue_id]) }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                                
                        <label for="item_name">Item Name</label><br>
                        <!-- <input type="text" name="item_name" id="item_name" value="{{ $issue->item_name }}" required class="form-control"><br> -->
                        <select name="item_name" class="form-control" id="item_name" required>
                            <option value="{{ $issue->item_name }}">{{ $issue->item_name }}</option>
                            <option value="Potato">Potato</option>
                            <option value="Tomato">Tomato</option>
                            <option value="Onion">Onion</option>
                            <option value="Chilli">Chilli</option>
                            @foreach ($categories->unique('item_name') as $category)
                                @if (!in_array($category->item_name, ['Potato', 'Tomato', 'Onion', 'Chilli', 'Mushroom']))
                                    <option value="{{ $category->item_name }}">{{ $category->item_name }}</option>
                                @endif
                            @endforeach
                                
                        </select><br>
                        <label for="quantity">Quantity</label><br>
                        <input type="text" name="quantity" id="quantity" value="{{ $issue->quantity }}" required class="form-control"><br>
                        <label for="unit">Unit</label><br>
                        <!-- <input type="text" name="unit" id="unit" value="{{ $issue->unit }}" required class="form-control"><br> -->
                        <select name="unit" id="unit" class="form-control" required>
                            
                            <option value="{{ $issue->unit }}">{{ $issue->unit }}</option>
                            <option value="gram">gram</option>
                                         
                            @foreach ($categories->unique('unit') as $category)
                                @if (!in_array($category->unit, ['kg', 'gram',]))
                                    <option value="{{ $category->unit }}">{{ $category->unit }}</option>
                                @endif
                            @endforeach

                        </select><br>
                    
                        <div style="align-items: center;">
                            <button type="submit" class="btn btn-success">Update</button>
                            <button type="button" class="btn btn-secondary" onclick="goBack()">Back</button>

                            <script>
                                function goBack() {
                                    window.history.back();
                                }
                            </script>
                        </div>
                    </form>

                  </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>

