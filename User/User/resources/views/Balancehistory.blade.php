<!DOCTYPE html>
<html lang="en">

<head>

    <title>GCIT Mess</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/xlsx/dist/xlsx.full.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/dashboard.css">

    <style>
        /* Center-align text */
        .text-left {
            text-align: left;
            text-decoration: underline
        }

        /* Right-align text */
        .text-right {
            text-align: right;
        }

        /* Create a container for the text */
        .text-container {
            display: flex;
            justify-content: space-between;
            margin: 0 15px;
            margin-bottom: -6px
        }
    </style>

</head>

<body>
    @include('sidebar')
    <br>

    <div class="container-fluid">
        <div class="row content">
            <div class="col-sm-9">
                <div class="topbar">
                    <h4> Balance History</h4>
                    <div>
                        <!-- <label for="searchMonth">Filter by Month:</label> -->
                        <select class="form-control" id="searchMonth">
                            <option value="">All Months</option>
                            <option value="jan">January</option>
                            <option value="feb">February</option>
                            <option value="mar">March</option>
                            <option value="apr">April</option>
                            <option value="may">May</option>
                            <option value="jun">June</option>
                            <option value="jul">July</option>
                            <option value="aug">August</option>
                            <option value="sep">September</option>
                            <option value="oct">October</option>
                            <option value="nov">November</option>
                            <option value="dec">December</option>
                        </select>
                    </div>
                </div>

                <br>

                @foreach ($historicalData as $data)
                    {{-- Check if there is any data for the current month --}}
                    @if (!empty($data['combinedItems']) || !empty($data['previousBalance']))
                    <div id="monthContainer{{ $loop->iteration }}" class="row col-sm-12">
                        <div class="text-container" >
                            <div class="text-left month-info">
                                <p style="font-size: 16px;"><b>{{ $data['month'] }}</b></p>
                            </div>

                        <div class="text-right">
                            <p>Balance History for month : {{ $data['month'] }}</p>
                        </div>
                        </div>

                            <div class="overflowx" style="border-radius: 10px; background: #FFF; margin-right: 20px;">

                                <table style="border: 1px solid rgba(0, 0, 0, 0.30); margin: 5px 0 0 22px; width: 100%;" class="table table-bordered">
                                    <thead style="background-color: rgba(58, 104, 68, 0.90); color: white;">
                                            <tr>
                                                <th style="text-align: center;">SL.NO</th>
                                                <th style="text-align: center;">Item Name</th>
                                                <th style="text-align: center;">Balance brought forward</th>
                                                <th style="text-align: center;">Purchased Item Quantity </th>
                                                <th style="text-align: center;">Unit</th>
                                                <th style="text-align: center;">Total Quantity Item</th>
                                                <th style="text-align: center;">Item Usage (Issued)</th>
                                                <th style="text-align: center;">Spoiled Item quantity</th>
                                                <th style="text-align: center;">Remaining Balance</th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data['previousBalance'] as $previousItem)
                                            @if (!isset($data['combinedItems'][$previousItem['item_name']]) && $previousItem['balance'] > 0)
                                                <tr>
                                                    <td style="text-align: center;">{{ $loop->index + 1 }}</td>
                                                    <td style="text-align: center;">{{ $previousItem['item_name'] }}</td>
                                                    <td style="text-align: center;">{{ $previousItem['balance'] }}</td>
                                                    <td style="text-align: center;">0</td>
                                                    <td style="text-align: center;">{{ $previousItem['unit'] }}</td>
                                                    <td style="text-align: center;">{{ $previousItem['balance'] }}</td>
                                                        
                                                    <td style="text-align: center;">0</td>
                                                    <td style="text-align: center;">0</td>
                                                    <td style="text-align: center;">{{ $previousItem['balance'] }}</td>
                                                </tr>
                                            @endif
                                        @endforeach

                                        @foreach ($data['combinedItems'] as $item)
                                            <tr>
                                                <td style="text-align: center;">{{ $loop->index + 1 }}</td>
                                                <td style="text-align: center;">{{ $item['item_name'] }}</td>
                                                <td style="text-align: center;">
                                                    @if (isset($data['previousBalance'][$item['item_name']]))
                                                        {{ $data['previousBalance'][$item['item_name']]['balance'] }}
                                                    @else
                                                        0
                                                    @endif
                                                </td>
                                                <td style="text-align: center;">{{ $item['total_quantity'] }}</td>
                                                <td style="text-align: center;">{{ $item['unit'] }}</td>
                                                <td style="text-align: center;">
                                                    @php
                                                        $newTotal = $item['total_quantity'] + (isset($data['previousBalance'][$item['item_name']]) ? $data['previousBalance'][$item['item_name']]['balance'] : 0);
                                                    @endphp
                                                    {{ $newTotal }}
                                                </td>
                                                    
                                                <td style="text-align: center;">{{ $item['issue_quantity'] }}</td>
                                                <td style="text-align: center;">{{ $item['spoiled_quantity'] }}</td>
                                                <td style="text-align: center;">
                                                    @php
                                                        $remainingBalance = $newTotal - $item['spoiled_quantity'] - $item['issue_quantity'];
                                                    @endphp
                                                        {{ $remainingBalance }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    @endif
                @endforeach
                
            </div>
        </div>
    </div>

    <script>
            $(document).ready(function () {
                // Initial visibility of all months
                $('[id^="monthContainer"]').show();

                // Dropdown change event
                $('#searchMonth').change(function () {
                    // Get the selected search month
                    var selectedMonth = $(this).val();

                    // Hide all months initially
                    $('[id^="monthContainer"]').hide();

                    // Show only the matching month or show all if "All Months" is selected
                    if (selectedMonth === '') {
                        $('[id^="monthContainer"]').show();
                    } else {
                        $('[id^="monthContainer"]').each(function () {
                            var monthText = $(this).find('.month-info p').text().toLowerCase();
                            if (monthText.includes(selectedMonth)) {
                                $(this).show();
                            }
                        });
                    }
                });
            });
        </script>


        <script>
        document.addEventListener("DOMContentLoaded", function () {
            document.querySelectorAll(".exportExcelBtn").forEach(function (button, index) {
                button.addEventListener("click", function () {
                    exportToExcel(index);
                });
            });

            function exportToExcel(index) {
                // Get the month information
                var monthInfo = document.querySelectorAll(".month-info")[index].textContent.trim();

                // Create a new workbook
                var wb = XLSX.utils.book_new();

                // Extract data from the specific table
                var ws = XLSX.utils.table_to_sheet(document.querySelectorAll("table")[index]);

                // Add the worksheet to the workbook
                XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

                // Save the workbook as an Excel file with the month information in the name
                XLSX.writeFile(wb, `Balance_History_of_${monthInfo}.xlsx`);
            }
        });
    </script>



    <script>
        // Function to format the date as "dd/mm/yyyy"
        function formatDate(date) {
            const day = date.getDate().toString().padStart(2, '0');
            const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-based
            const year = date.getFullYear();
            return `${day}/${month}/${year}`;
        }

        // Get the element by its ID
        const currentDateElement = document.getElementById('currentDate');

        // Update the date content
        const currentDate = formatDate(new Date());
        currentDateElement.textContent = `Date: ${currentDate}`;
    </script>

</body>

</html>
