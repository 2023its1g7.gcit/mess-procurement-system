
@extends('auth.layout')
@section('content')


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<style>
  button:hover {
    color: green; /* Change to the desired text color on hover */
    border: 2px solid green; /* Change to the desired border color on hover */
  }

  button a {
    color: #417A4E; /* Set the initial text color */
    text-decoration: none;
  }
</style>
<main class="login-form">
<div style="position: absolute; top:30px; left:30px;">
    
    <a href="/" style="color: white; text-decoration: none;">
    <i class="fas fa-arrow-left"></i>  Back
    </a>
    
  </div>
  <div class="cotainer">
  
      <div class="row justify-content-center">
     
          <div class="col-md-3">
              <div class="card" style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
                  <div class="text-center" 
                  style=" background-color:transparent;font-family: 'Roboto', sans-serif;
                  border-bottom:white;font-size:20px;color:white">Reset Password</div>
                  <div class="card-body">
  
                    @if (Session::has('message'))
                         <div class="alert alert-success" role="alert">
                            {{ Session::get('message') }}
                        </div>
                    @endif
  
                      <form action="{{ route('ForgetPasswordPost') }}" method="POST" >
                          @csrf
                          <div class="form-group row">
                              <label for="email_address" class=" col-form-label text-md-right"></label>
                              <div class="col-md-12">
                                  <input type="text" id="email_address"  placeholder="E-mail Address" class="form-control" name="email" required autofocus>
                                  @if ($errors->has('email'))
                                      <span class="text-danger">{{ $errors->first('email') }}</span>
                                  @endif
                              </div>
                          </div>
                          <div class="col-md-12">
                          
                              <button class="w-100 btn btn-md custom-btn-color" type="submit">
                               
                                <p class="card-text text-center">
                                    Send Reset Password Link
                                </p>    
                            </button>
                          </div>
                      </form>
                        
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
<style>
    
    
.custom-btn-color {
    background-color: #417A4E;
    transition: background-color 0.3s, color 0.3s; /* Add a smooth transition effect */
    border: 2px solid transparent;
    color:white; 
    font-family: 'Roboto', sans-serif;
}

.custom-btn-color:hover {
    background-color: white; /* Change background color to white on hover */
    color:#417A4E; /* Change font color to black on hover */

    border-color:#417A4E;
    font-family: 'Roboto', sans-serif;
    
}
.custom-btn-color:focus {

    border-color: #417A4E; /* Change the border color to green on focus */
   
}
.card {
    position: relative;
    z-index: 1;
    padding: 20px;
    background: rgba(255, 255, 255, 0.2); /* Transparent white background for the frosted glass effect */
    backdrop-filter: blur(10px); /* Apply a blur filter to simulate frosted glass */
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
    border-radius:8px;
}
.form-control:focus {
    border-color:#417A4E; /* Set the border color to green when in focus */
    box-shadow: 0 0 0 0.2rem rgba(0, 128, 0, 0.25); /* Add a green shadow when in focus */
}


</style>
@endsection


