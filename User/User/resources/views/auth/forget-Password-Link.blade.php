@extends('auth.layout')
  
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<style>
  button:hover {
    color: green; /* Change to the desired text color on hover */
    border: 2px solid green; /* Change to the desired border color on hover */
  }

  button a {
    color: #417A4E; /* Set the initial text color */
    text-decoration: none;
  }
</style>

<main class="login-form">
<div style="position: absolute; top:30px; left:30px;">
    
    <a href="/" style="color: white; text-decoration: none;">
    <i class="fas fa-arrow-left"></i>  Back
    </a>
    
  </div>
  <div class="cotainer">
      <div class="row justify-content-center">
          <div class="col-md-3">
              <div class="card">
                  <div class="card-header text-center" 
                  style=" background-color:transparent;font-family: 'Roboto', sans-serif;
                  border-bottom:white;font-size:20px;color:white">Reset Password</div>
                  <div class="card-body">
  
                      <form action="{{ route('ResetPasswordPost') }}" method="POST">
                          @csrf
                          <input type="hidden" name="token" value="{{ $token }}">
  
                          <div class="form-group row">
                            
                             
                              <div class="col-md-12">
                                  <input type="text" id="email_address" placeholder="E-Mail Address" class="form-control" name="email" required autofocus>
                                 
                                  @if ($errors->has('email'))
                                      <span class="text-danger">{{ $errors->first('email') }}</span>
                                  @endif
                              </div>
                          </div>


                          <div class="input-group mb-3 col-md-12">
        <input  style="border-right:0px solid white;" type="password" placeholder="Password" id="password" class="form-control" name="password" required autofocus>
        <span class="input-group-text" style="background-color:white;border-radius:0px 6px 6px 0px; border-left:0px solid white; height:38px">
          <i id="togglePassword" class="fa fa-eye" aria-hidden="true"></i>
        </span>
        @if ($errors->has('password'))
          <span class="text-danger">{{ $errors->first('password') }}</span>
        @endif
      </div>

      <div class="input-group mb-3 col-md-12">
        <input style="border-right:0px solid white;"  type="password" placeholder="Confirm Password" id="password-confirm" class="form-control" name="password_confirmation" required autofocus>
        <span class="input-group-text" style="background-color:white;border-left:0px solid white;height:38px;border-radius:0px 6px 6px 0px">
          <i id="togglecon" class="fa fa-eye" aria-hidden="true"></i>
        </span>
        @if ($errors->has('password_confirmation'))
          <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
        @endif
      </div>
                         
                          
                          <div class="col-md-12">
                          
                          <button class="w-100 btn btn-md custom-btn-color" type="submit">
                           
                            <p class="card-text text-center">
                                Reset Password
                            </p>    
                        </button>
                      </div>
                      </form>
                        
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
<script>
    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password');

    togglePassword.addEventListener('click', function () {
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        this.classList.toggle('fa-eye');
        this.classList.toggle('fa-eye-slash');
    });
  </script>

  <script>
    const togglePassword1 = document.querySelector('#togglecon');
    const passwordconf = document.querySelector('#password-confirm');

    togglePassword1.addEventListener('click', function () {
        const type = passwordconf.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordconf.setAttribute('type', type); // Change 'password' to 'passwordconf'
        this.classList.toggle('fa-eye');
        this.classList.toggle('fa-eye-slash');
    });
  </script>


<style>
    
    
    .custom-btn-color {
        background-color: #417A4E;
        transition: background-color 0.3s, color 0.3s; /* Add a smooth transition effect */
        border: 2px solid transparent;
        color:white; 
        font-family: 'Roboto', sans-serif;
    }
    
    .custom-btn-color:hover {
        background-color: white; /* Change background color to white on hover */
        color:#417A4E; /* Change font color to black on hover */
    
        border-color:#417A4E;
        font-family: 'Roboto', sans-serif;
        
    }
    .custom-btn-color:focus {
    
        border-color: #417A4E; /* Change the border color to green on focus */
       
    }
    .card {
        position: relative;
        z-index: 1;
        padding: 20px;
        background: rgba(255, 255, 255, 0.2); /* Transparent white background for the frosted glass effect */
        backdrop-filter: blur(10px); /* Apply a blur filter to simulate frosted glass */
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
        
    }
    .form-control:focus {
    border-color:#417A4E; /* Set the border color to green when in focus */
    box-shadow: 0 0 0 0.2rem rgba(0, 128, 0, 0.25); /* Add a green shadow when in focus */
}
</style>
@endsection