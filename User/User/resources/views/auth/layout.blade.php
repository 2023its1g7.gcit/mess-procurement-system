<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" crossorigin="anonymous">
        <style type="text/css">
            @import url(https://fonts.googleapis.com/css?family=Raleway:300,400,600);
    
            .navbar-laravel {
                box-shadow: 0 2px 4px rgba(0,0,0,.04);
            }
            .navbar-brand , .nav-link, .my-form, .login-form {
                font-family: Raleway, sans-serif;
            }
            .my-form {
                padding-top: 1.5rem;
                padding-bottom: 1.5rem;
                
            }
            .my-form .row {
                margin-left: 0;
                margin-right: 0;
            }
            .login-form {
                padding-top: 20rem;
               
            }
            .login-form .row {
                margin-left: 0;
                margin-right: 0;
            }
            .blur-background {
    position: relative;
    z-index: 0;
}

.blur-background::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    height:100vh;
    background-image: linear-gradient(to bottom,
     rgba(0, 0, 0, 0.4), 
     rgba(0, 0, 0, 0.4)),
      url('/img/3.jpg'); /* Specify the path to your background image */
    background-size: cover;
    background-repeat: no-repeat;
    background-attachment: fixed;
    filter: blur(3px); /* Adjust the blur amount as needed */
    z-index: -1;
    
}
        </style>
    </head>
<body class="blur-background">
    

@yield('content')
     
</body>
</html>