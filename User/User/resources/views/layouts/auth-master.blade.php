<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.87.0">
    <title>LOGIN</title>

    <!-- Bootstrap core CSS -->
    <link href="{!! url('assets/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! url('assets/css/signin.css') !!}" rel="stylesheet">
    
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
</head>
<body class="text-center blur-background">
    
    <main class="form-signin" >

        @yield('content')
        
    </main>
    

</body>
<style>
 .blur-background {
    position: relative;
    z-index: 0;
}

.blur-background::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-image: linear-gradient(to bottom,
     rgba(0, 0, 0, 0.4), 
     rgba(0, 0, 0, 0.4)),
      url('/img/3.jpg'); /* Specify the path to your background image */
    background-size: cover;
    background-repeat: no-repeat;
    background-attachment: fixed;
    filter: blur(3px); /* Adjust the blur amount as needed */
    z-index: -1;
    
}
</style>
</html>
