<style>
    /* Modal Styles */
.modal {
    display: none;
    position: fixed;
    z-index: 1;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgba(0, 0, 0, 0.7);
}

.modal-content {
    background-color: #fff;
    margin: 10% auto;
    padding: 20px;
    border: 1px solid #ccc;
    border-radius: 5px;
    max-width: 400px; /* Adjust the maximum width as needed */
    width: 90%;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    text-align: center;
}

h2 {
    font-size: 1.6rem;
    margin-bottom: 15px;
    font-family: 'Roboto', sans-serif;
}

p {
    font-size: 1.5rem;
    margin-bottom: 20px;
    font-family: 'Roboto', sans-serif;
}

/* Buttons */
button {
    padding: 10px 20px;
    margin: 0 10px;
    cursor: pointer;
    border: none;
    border-radius: 5px;
}

#confirmLogoutButton {
    background-color: #ff4d4d;
    color: #fff;
}

#cancelLogoutButton {
    background-color: #ccc;
    color: white;
}


@media screen and (max-width: 600px) {
    .modal-content {
        width: 95%;
    }
}

</style>  

<nav class="navbar navbar-inverse visible-xs" style="background-color: rgba(58, 104, 68, 0.90);">
    <div class="container-fluid">
        <div class="navbar-header"  >
            <button type="button" class="navbar-toggle" style="background-color: transparent; border: 1px solid white;" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand topic" href="" style="color:white">GCIT Mess Procurement</a>
        </div>
        <div class="collapse navbar-collapse collapNav" id="myNavbar">
            <ul class="nav navbar-nav">
            <li class="clink" id="product-link " style="margin: 5px; ">
                    <a style="color: #FFF;" href="/dashboard">
                    <i class="fas fa-th-large" style="margin-right: 5px;"></i> Dashboard
                    </a>
                </li>

                <li class="clink"  style="margin: 5px; ">
                    <a style="color: #FFF;" href="/product">
                        <i class="fas fa-shopping-cart" style="margin-right: 5px;"></i> Purchased List
                    </a>
                </li>

                <li class="clink" id="customers-link" style="margin: 5px;">
                    <a style="color: #FFF;" href="/issuelist">
                        <i class="fas fa-users" style="margin-right: 5px;"></i> Issued List
                    </a>
                </li>

                <li class="clink" id="customers-link" style="margin: 5px;">
                    <a style="color: #FFF;" href="/spoillist">
                        <i class="fas fa-users" style="margin-right: 5px;"></i> Update Spolied Item
                    </a>
                </li>

                <li class="clink" id="feedback-link" style="margin: 5px;">
                    <a style="color: #FFF;" href="/balancecheck">
                        <i class="fas fa-comments" style="margin-right: 5px;"></i> Item Balanced Check
                    </a>
                </li>

                <li class="clink"  style="margin: 5px; ">
                    <a style="color: #FFF;" href="balancehistory">
                        <i class="fas fa-shopping-cart" style="margin-right: 5px;"></i> Balanced History
                    </a>
                </li>
             
                <hr style="width:auto; margin: 15px 5px;"> 
                <li class="clink"  style="margin: 5px; ">
                    <a style="color: #FFF;" href="#">
                    <i class="fas fa-th-large" style="margin-right: 5px;"></i>Logout
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<div class="col-sm-3 hidden-xs text-center sidenav">

    <img src="{{ asset('img/logo.png') }}" alt="Image Alt Text" style="max-width: 120px; max-height:120px; margin-right: 20px;margin-left: 20px; margin-top:30px">

        <h2 class="topic">GCIT Mess Procurement</h2>
        <h3 class="topic">Welcome Back</h3>

        <hr class="hr">
            <ul class="nav nav-pills nav-stacked">

                <li class="link"  style="margin-top: 15px; ">
                    <a style="color: #FFF;" href="/dashboard">
                    <i class="fas fa-th-large" style="margin-right: 5px;"></i> Dashboard
                    </a>
                </li>

                <li class="link"  style="margin-top: 15px; ">
                    <a style="color: #FFF;" href="/product">
                        <i class="fas fa-shopping-cart" style="margin-right: 5px;"></i> Purchased List
                    </a>
                </li>
                <li class="link"  style="margin-top: 15px;">
                    <a style="color: #FFF;" href="issuelist">
                        <i class="fas fa-users" style="margin-right: 5px;"></i> Issued List
                    </a>
                </li>

                <li class="link"  style="margin-top: 15px;">
                    <a style="color: #FFF;" href="/spoillist">
                        <i class="fas fa-sync" style="margin-right: 5px;"></i> Update Spoiled Item
                    </a>
                </li>

          
                <li class="link"  style="margin-top: 15px;">
                    <a style="color: #FFF;" href="balancecheck">
                        <i class="fas fa-balance-scale" style="margin-right: 5px;"></i> Item Balanced Check
                    </a>
                </li>


                <li class="link"  style="margin-top: 15px; ">
                    <a style="color: #FFF;" href="balancehistory">
                        <i class="fas fa-history" style="margin-right: 5px;"></i> Balanced History
                    </a>
                </li>

                <br>
                <hr style="padding-bottom: 10px;" class="hr"> 
                <li class="link"  style="margin-top: 15px;">
                    <a style="color: #FFF;" href="{{ route('logout.perform') }}" id="logoutButton" >
                        <i class="fas fa-sign-out-alt" style="margin-right: 5px;"></i> Logout
                    </a>
                </li>

        </ul>

    <br>

</div>

<div id="logoutModal" class="modal">
    <div class="modal-content">
        <h2>Logout Confirmation</h2>
        <p>Are you sure you want to log out?</p>
        <button id="confirmLogoutButton">Yes</button>
        <button id="cancelLogoutButton">No</button>
    </div>
</div>

<script>
 
function openModal() {
    var modal = document.getElementById('logoutModal');
    modal.style.display = 'block';
}

// Function to close the modal
function closeModal() {
    var modal = document.getElementById('logoutModal');
    modal.style.display = 'none';
}

// Add event listeners
document.getElementById('logoutButton').addEventListener('click', function(event) {
    event.preventDefault(); // Prevent the default link behavior
    openModal();
});

document.getElementById('confirmLogoutButton').addEventListener('click', function() {
    // Perform the logout action when the user confirms
    window.location.href = "{{ route('logout.perform') }}";
});

document.getElementById('cancelLogoutButton').addEventListener('click', function() {
    closeModal();
});

// Close the modal if the user clicks anywhere outside of it
window.addEventListener('click', function(event) {
    var modal = document.getElementById('logoutModal');
    if (event.target === modal) {
        closeModal();
    }
});

</script>

<style>
    .active {
    background-color: #4CAF50; 
}
</style>
<script>
    $(document).ready(function(){
    $('ul.navbar-nav a, ul.nav-pills.nav-stacked a').each(function() {
        if (this.href == window.location.href) {
            $(this).closest('li').addClass('active');
        }
    });
});

</script>