<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GCIT Mess</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./css/dashboard.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

   

</head>

<style>
.chart-title {
    text-align: center;
}

.chart-title p {
    font-size: 20px;
    
}
</style>

<body>
    @include('sidebar')
    <br>

    <div class="container-fluid">
        <div class="row content">
            <div class="col-sm-9">
                <div class="topbar">
                    <h4>Dashboard</h4>
                </div>

                <div class="col-sm-4" style="padding-top: 15px;">
                <div class="threebox" style="padding: 40px 0;">
                  <div style="display: flex; align-items: center;">
                    <img src="{{ asset('img/1.png') }}" alt="Image Alt Text" style="max-width: 50px; max-height: 50px; margin-right: 20px;margin-left: 20px;">
                      <div>

                        <p style="font-size:17px">Previous Monthly Spent</p>
                        <h4>Nu.{{ $previousMonthAmount }}</h4>
                       
                      </div>
                  </div>
                  </div>
              </div>
              <div class="col-sm-4" style="padding-top: 15px;">
                <div class="threebox" style="padding: 40px 0;">
                  <div style="display: flex; align-items: center;">
                      <img src="{{ asset('img/2.png') }}" alt="Image Alt Text" style="max-width: 50px; max-height: 50px; margin-right: 20px;margin-left: 20px;">
                      <div>
                        
                        <p style="font-size:17px">Current Amount Spent</p>
                        <h4>Nu.{{ $currentMonthAmount }}</h4>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="padding-top: 15px;">
                <div class="threebox" style="padding: 40px 0;">
                  <div style="display: flex; align-items: center;">
                      <img src="{{ asset('img/3.jpeg') }}" alt="Image Alt Text" style="max-width: 50px; max-height: 50px; margin-right: 20px;margin-left: 20px;">
                      <div>

                        <p style="font-size:17px">Monthly Average Spent</p>
                        <h4>Nu.{{ $averageAmount }}</h4>
                        
                      </div>
                  </div>
                </div>
              </div>

                <div class="col-sm-12">
                  <div style="border-radius: 10px; border: 1px solid rgba(0, 0, 0, 0.30); background: #FFF; margin-top:20px; height: auto; flex-shrink: 0; padding:30px;" >
                    
                  <script type="text/javascript">
                        window.onload = function () {
                            var chart = new CanvasJS.Chart("chartContainer", {
                                title: {
                                    text: "Amount Spent - per month"
                                },
                                axisX: {
                                    title: "Month",
                                    valueFormatString: "MMM",
                                    interval: 1,
                                    intervalType: "month"
                                },
                                axisY: {
                                    title: "Amount (Nu.)",
                                    includeZero: false
                                },
                                data: [
                                    {
                                        type: "line",
                                        dataPoints: [
                                            @foreach($xValues as $key => $month)
                                                { x: new Date({{ date('Y', strtotime($month)) }}, {{ date('m', strtotime($month)) - 1 }}, 1), y: {{ $yValues[$key] }} },
                                            @endforeach
                                        ]
                                    }
                                ]
                            });

                            chart.render();
                        }
                    </script>

                    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                    <script type="text/javascript" src="https://cdn.canvasjs.com/canvasjs.min.js"></script>
                    </script>

                  </div>
                </div>
                   
                <div class="row">

                    <div class="col-md-6">
                        
                        <div style="border-radius: 10px; border: 1px solid rgba(0, 0, 0, 0.30); background: #FFF; margin:14px; height: auto; flex-shrink: 0; padding:15px; box-shadow: 4px 6px 8px rgba(0, 0, 0, 0.1);">
                        <div class="chart-title">
                            <p><b>Top 5 Most Used Items </b></p>
                        </div>
                            
                            <canvas id="monthlyGraph" style="width:100%; max-width:600px; height:280px"></canvas>

                            <script>
                            // Get the data passed from the controller
                            var itemNames = @json($itemNames); // An array of item names
                            var quantities = @json($quantities); // An array of quantities

                            // Sort the items by quantity in descending order
                            var sortedData = itemNames.map((item, index) => ({ name: item, quantity: quantities[index] }))
                                .sort((a, b) => b.quantity - a.quantity)
                                .slice(0, 5); // Get the top 5 items

                            // Separate item names and quantities
                            var top5ItemNames = sortedData.map(item => item.name);
                            var top5Quantities = sortedData.map(item => item.quantity);

                            var ctx = document.getElementById("monthlyGraph").getContext('2d');

                            new Chart(ctx, {
                                type: 'bar',
                                data: {
                                    labels: top5ItemNames, // X-axis labels (item names)
                                    datasets: [{
                                        label: 'Top 5 Items used by Quantity',
                                        data: top5Quantities, // Y-axis data (quantities)
                                        backgroundColor: 'rgba(106, 151, 249, 1)', // Bar color
                                        borderWidth: 1
                                    }]
                                },
                                options: {
                                    scales: {
                                        y: {
                                            beginAtZero: true,
                                            title: {
                                                display: true,
                                                text: 'Quantity (kg)'
                                            }
                                        },
                                        x: {
                                            title: {
                                                display: true,
                                                text: 'Item Names'
                                            }
                                        }
                                    }
                                }
                            });
                        </script>

                            
                        </div>
                    </div>
                    @foreach ($historicalData as $data)
                    <div class="col-md-6">
                        <div style="display: flex; flex-direction: column; align-items: center; height: 39vh; border-radius: 10px; border: 1px solid rgba(0, 0, 0, 0.30); margin: 14px; padding: 15px; box-shadow: 4px 6px 8px rgba(0, 0, 0, 0.1);">
                            <h4><b>Item in Low stock</b></h4>
                            @php
                                $customIndex = 1;
                            @endphp
                            <table>
                                <thead>
                                    <tr>
                                        <th  style="text-align: center;">Sl.No</th>
                                        <th>Item Name</th>
                                        <!-- <th style="text-align: center;">Threshold Value</th> -->
                                        <th style="text-align: center;">Remaining Balance</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data['combinedItems'] as $item)
                                        @php
                                            $newTotal = $item['total_quantity'] + (isset($data['previousBalance'][$item['item_name']]) ? $data['previousBalance'][$item['item_name']]['balance'] : 0);
                                            $remainingBalance = $newTotal - $item['spoiled_quantity'] - $item['issue_quantity'];
                                        @endphp

                                        @foreach ($categories as $threshold)
                                            @if ($threshold['item_name'] == $item['item_name'] && $remainingBalance < $threshold['threshold'])
                                                <tr>
                                                    <td  style="text-align: center;">{{ $customIndex }}</td>
                                                    <td>{{ $item['item_name'] }}</td>
                                                    <!-- <td style="text-align: center;">{{ $threshold['threshold'] }}</td> -->
                                                    <td style="text-align: center;">{{ $remainingBalance }}</td>
                                                </tr>
                                                @php
                                                    $customIndex++;
                                                @endphp
                                            @endif
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach
                    
            </div>
    </body>
</html>
