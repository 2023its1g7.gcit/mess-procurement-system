

<!DOCTYPE html>
<html lang="en">

<head>

  <title>GCIT Mess</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user.css') }}">

</head>

<body>
  @include('sidebar')
  <br>
  
  <div class="container-fluid">
    <div class="row content">

      <div class="col-sm-9">
          <div  class="topbar">
              <h4>Issued List</h4>   
              <div class="date-search" style="display: flex;">
              <form action="{{ route('issuelist') }}" method="get">
                @csrf
                <div class="date-search" style="display: flex;">
                    <input type="date" class="form-control" name="date" id="datepicker"  value="{{ request('date') }}" style="border-top-right-radius: 0; border-bottom-right-radius: 0;">
                    <button type="submit" class="btn btn-primary" style="border-top-left-radius: 0; border-bottom-left-radius: 0;background-color:#4B8B5A;border:1px solid #4B8B5A">
                    <i class="fa fa-search"></i>
                    </button>
                </div>
            </form>

            </div>
          </div>
          <br>
          <div class="text-container">
            <div class="text-left">
                <!-- <p>Date : 02/10/2023</p> -->

            </div>

            <div class="text-center">
                <!-- <p>Line in the Middle</p> -->
            </div> 

            <div class="text-right">
              <a href="/addissue"> 
                <button class="text-bt">
                    <i class="fa fa-plus"></i> Add Issue list
                </button></a>
              
            </div>

          </div>

          @foreach($groupedIssues as $date => $vendorIssues)
            <br>
            @php
                $meal = null;
                $submitter = null;

                // Check if there are any vendor issues to fetch meal and submitter from
                if (count($vendorIssues) > 0 && is_object($vendorIssues[0])) {
                    $firstIssue = $vendorIssues[0];

                    // Fetch meal and submitter from the first issue
                    $meal = $firstIssue->meal;
                    $submitter = $firstIssue->submitter;
                }
            @endphp

            <!-- Display "Submitter" and "Meal" fields above the loop -->
            <div class="text-container">
                <div class="text-left">
                    <!-- Display the submitter -->
                    <p><b>Date :</b> {{ $date }}</p>
                </div>

                <div class="text-center">
                    <p><b>Meal:</b> {{ $meal }}</p>
                </div>

                <div class="text-right">
                    <!-- Display the meal -->
                    <p><b>Submitted by :</b> {{ $submitter }}</p>
                </div>
            </div>

            <!-- Update the table header to include columns for "Submitter" and "Meal" -->
            <div class="table-container">
                <table>
                    <thead style="background: #4B8B5A; color: white;">
                        <tr>
                            <th style="text-align: center;">Sl No.</th>
                            <th>Item Name</th>
                            <th style="text-align: center;">Quantity</th>
                            <th style="text-align: center;">Unit</th>
                            <th style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    @foreach($vendorIssues as $issue)
                        @if(is_object($issue))
                            <tr>
                                <td style="text-align: center;">{{ $issue->sl_no }}</td>
                                <td>{{ $issue->item_name }}</td>
                                <td style="text-align: center;">{{ $issue->quantity }}</td>
                                <td style="text-align: center;">{{ $issue->unit }} </td>
                                <td style="text-align: center;">
                                    @php
                                        $currentDate = now()->format('Y-m-d');
                                        $issueDate = $issue->created_at->format('Y-m-d');
                                    @endphp

                                    @if ($issueDate == $currentDate)
                                        <a href="{{ route('editIssue', ['issue_id' => $issue->issue_id]) }}" title="Edit Issue">
                                            <button class="btn btn-primary btn-sm mr-2" data-toggle="modal" data-target="#flipFlop">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit 
                                            </button>
                                        </a>
                                    @else
                                        <button class="btn btn-primary btn-sm mr-2" disabled title="You cannot edit past data">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit 
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
        @endforeach
    </div>
  </div>

  

</body>
</html>


