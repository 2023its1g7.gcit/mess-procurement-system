<!DOCTYPE html>
<html lang="en">

<head>

  <title>GCIT Mess</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">

  <style>

  /* Create a container for the text */
  .text-container {
      display: flex;
      justify-content: space-between;
      margin:0 15px 0px 15px;
      padding: 15px 0 15px 0
      
  }
  
  </style>
</head>

<body>
  @include('sidebar')
  <br>
  
  <div class="container-fluid">
    <div class="row content">

      <div class="col-sm-9">
          <div  class="topbar">
              <h4>Add Issue List</h4>   
          </div>

          <form action="{{ route('issuelist') }}" method="post">
          @csrf
          @method('post')

            <div class="text-container">
              <div class="text-left">
             
                  <select name="submitter[]" class="form-control" required>
                    <option value="">--Select submitter--</option>
                        @foreach($users as $user)
                            <option value="{{ $user->name }}">{{ $user->name }}</option>
                        @endforeach
                  </select>
              </div>

              <div class="text-center" style="margin-top:4px">
                  <select name="meal[]" class="form-control" required>
                      <option value="">--Select meal--</option>
                      <option value="Breakfast">Breakfast</option>
                      <option value="Lunch">Lunch</option>
                      <option value="Dinner">Dinner</option>
                  </select>
                  <!-- <p>Line in the Middle</p> -->
              </div> 

              <div class="text-right">
              
                <!-- <button class="text-bt" data-toggle="modal" data-target="#flipFlop">
                    <i class="fa fa-plus"></i> Add category
                </button> -->
              
            </div>
            </div>
                <div class="table-container">
                    <table id="dynamic-table">
                        <thead style="background: #4B8B5A; color: white;">
                        <tr>
                            <th>Sl No.</th>  
                            <th>Item Name</th>
                            <th>Quantity Issued</th>
                            <th>Unit</th>
                            <th style="text-align:center">Action</th>
                        </tr>
                        </thead>
                        
                        <tr>
                            <td><input type="number" name="sl_no[]" id="sl-no-1" class="sl-no-field" value="1" required /></td>
                            
                            <td>
                            <select name="item_name[]" class="form-control" required>
                                <option value="">-- Select Product Name --</option>

                                <option value="Onion">Onion</option>
                                @foreach ($categories->unique('item_name') as $category)
                                    @if (!in_array($category->item_name, ['Onion',]))
                                        <option value="{{ $category->item_name }}">{{ $category->item_name }}</option>
                                    @endif
                                @endforeach
                            </select>

                            </td>


                            <td><input type="number" step="0.01" name="quantity[]" required /></td>
                            
                            <td>
                            <select name="unit[]" class="form-control" required>
                                <!-- <option value="">--Select Unit--</option> -->
                                <option value="kg">kg</option>

                                @foreach ($categories->unique('unit') as $category)
                                    @if (!in_array($category->unit, ['kg']))
                                        <option value="{{ $category->unit }}">{{ $category->unit }}</option>
                                    @endif
                                @endforeach
                            </select>


                            </td>
                            <td style="text-align:center">
                                <button class="action-button" style="color:green" type="button" onclick="addRow()">Add row</button>
                            </td>
                            
                        </tr>
                    </table>
                    
                </div>
                <div class="text-container">
                <div class="text-left">
                    <input class="reciept" type="submit" value="Submit Data" />
                    <!-- <input class="reciept" type="submit" value="Submit Data" onclick="this.disabled = true; this.form.submit();" /> -->

                </div>

                <div class="text-right">
                    <div class="row" style="margin-right:3px">
                        
                        <!-- <button type="button" onclick="addRow()" class="reciept">
                            Add row
                        </button> -->
                    </div>
                </div>
            </div>
            </form>

            <!-- The modal -->
            <div class="modal fade" id="flipFlop" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalLabel">Add Category</h4>
                    </div>

                    <div class="modal-body">
                    <form action="{{ route('storecategory') }}" method="post">
                        @csrf
                        @method('post')
                    
                        <input style="Height:40px; border-radius:5px; border:1px solid #73AE64" type="text" name="item_name" placeholder="Item Name" required>
                        <br><br>
                        
                        <input style="Height:40px; border-radius:5px; border:1px solid #73AE64" type="text" name="unit" placeholder="Unit" required>
                        <br><br>
                        
                        <input style="Height:40px; border-radius:5px; border:1px solid #73AE64" type="number" name="threshold" placeholder="Minimum Stock Level" required>
                        <br><br>
                        <!-- <button type="submit">Submit</button> -->
                        
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </form>

                    </div>
                </div>
            </div>
        
            </div>
        </div>

        <script>

            
            var rowCount = 1; // Initialize a counter for Sl No

            function addRow() {
                var table = document.getElementById("dynamic-table");
                var newRow = table.insertRow(-1); // Insert at the end (after the last row)

                // Increment the counter for Sl No and set it in the new row's input field
                rowCount++;

                // Create the select element for Product Name
                var productSelect = document.createElement("select");
                productSelect.name = "item_name[]";
                productSelect.className = "form-control";
                productSelect.required = true;

                // Create the default option for Product Name
                var defaultProductOption = document.createElement("option");
                defaultProductOption.value = "";
                defaultProductOption.text = "--Select Product Name--";
                productSelect.appendChild(defaultProductOption);

                // Fetch and populate unique item names
                fetchUniqueItemNames(productSelect);

                // Create the select element for Unit
                var unitSelect = document.createElement("select");
                unitSelect.name = "unit[]";
                unitSelect.className = "form-control";
                unitSelect.required = true;

                // Create the default option for Unit
                var defaultUnitOption = document.createElement("option");
                defaultUnitOption.value = "kg";
                defaultUnitOption.text = "kg";
                unitSelect.appendChild(defaultUnitOption);

                // Fetch and populate unique units, excluding those in the static list
                fetchUniqueUnits(unitSelect);

                newRow.innerHTML = '<td><input type="number" name="sl_no[]" id="sl-no-' + rowCount + '" class="sl-no-field" value="' + rowCount + '" required /></td>' +
                    '<td></td>' + // Leave an empty td for the product select
                    '<td><input type="number" name="quantity[]" required /></td>' +
                    '<td></td>' + // Leave an empty td for the unit select
                    '<td style="font-size:24px; text-align:center"><a href="#" style="color: green; margin-right: 15px; text-decoration: none;" onclick="addRow()">+</a><a href="#" style="text-decoration: none; color:red" onclick="deleteRow(this)">-</a>';

                // Set the select elements in the new row
                newRow.cells[1].appendChild(productSelect);
                newRow.cells[3].appendChild(unitSelect);

                // Update the "Sl No" values for all rows
                updateSlNoValues();
            }

                function fetchUniqueUnits(unitSelect) {
                // Make an AJAX request to your Laravel route to fetch unique unit values
                fetch('/api/getUniqueUnits')
                    .then((response) => response.json())
                    .then((data) => {
                        // Populate the unitSelect dropdown with unique unit values
                        data.forEach((unitValue) => {
                            var unitOption = document.createElement("option");
                            unitOption.value = unitValue;
                            unitOption.text = unitValue;
                            unitSelect.appendChild(unitOption);
                        });
                    })
                    .catch((error) => {
                        console.error('Error fetching data:', error);
                    });
            }

            // Function to fetch unique item names from the server and populate the product select dropdown
            function fetchUniqueItemNames(productSelect) {
                // Make an AJAX request to your Laravel route to fetch unique item names
                fetch('/api/getUniqueItemNames')
                    .then((response) => response.json())
                    .then((data) => {
                        // Populate the productSelect dropdown with unique item names
                        data.forEach((itemValue) => {
                            var itemOption = document.createElement("option");
                            itemOption.value = itemValue;
                            itemOption.text = itemValue;
                            productSelect.appendChild(itemOption);
                        });
                    })
                    .catch((error) => {
                        console.error('Error fetching data:', error);
                    });
            }


            // Helper function to generate product options for the select element
            function getProductOptions(options) {
                var optionHTML = '';
                for (var i = 0; i < options.length; i++) {
                    optionHTML += '<option value="' + options[i].value + '">' + options[i].text + '</option>';
                }
                return optionHTML;
            }

            function deleteRow(button) {
                var table = document.getElementById("dynamic-table");
                var row = button.parentNode.parentNode;
                table.deleteRow(row.rowIndex);

                // Update the "Sl No" values for all rows after deleting a row
                updateSlNoValues();
            }

            function updateSlNoValues() {
                var slNoFields = document.getElementsByClassName("sl-no-field");
                for (var i = 0; i < slNoFields.length; i++) {
                    slNoFields[i].value = i + 1;
                }
            }

        </script>


    </body>

</html>
