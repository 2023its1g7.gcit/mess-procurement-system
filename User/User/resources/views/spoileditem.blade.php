
<!DOCTYPE html>
<html lang="en">

<head>

  <title>GCIT Mess</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user.css') }}">

</head>

<body>
  @include('sidebar')
  <br>
  
  <div class="container-fluid">
    <div class="row content">

      <div class="col-sm-9">
          <div  class="topbar">
              <h4>Spoiled List</h4>   
              <div class="date-search" style="display: flex;">
                <input type="date" class="form-control datepicker" id="datepicker" data-mdb-inline="true" value="2023-10-06" style="border-top-right-radius: 0; border-bottom-right-radius: 0;">
                <button class="btn btn-primary" style="border-top-left-radius: 0; border-bottom-left-radius: 0;background-color:#4B8B5A;border:1px solid #4B8B5A">
                  <i class="fa fa-search"></i>
                </button>
              </div>
          </div>
          <br>
          <div class="text-container">
            <div class="text-left">
                <!-- <p>Date : 02/10/2023</p> -->

            </div>

            <div class="text-center">
                <!-- <p>Line in the Middle</p> -->
            </div> 

            <div class="text-right">
              <a href="/addspoileditem"> 
                <button class="text-bt" style="font-size:0.9em">
                    <i class="fa fa-plus" ></i> Add Spoiled Item
                </button></a>
              
            </div>

          </div>

          @foreach($groupedspoils as $date => $vendorSpoils)
          <br>
            @php
              
                $submitter = null;

                // Check if there are any vendor Spoils to fetch meal and submitter from
                if (count($vendorSpoils) > 0 && is_object($vendorSpoils[0])) {
                    $firstIssue = $vendorSpoils[0];

                    // Fetch meal and submitter from the first issue
                    
                    $submitter = $firstIssue->submitter;
                }
            @endphp

        <!-- Display "Submitter" and "Meal" fields above the loop -->
        <div class="text-container">
            <div class="text-left">
                <!-- Display the submitter -->
                <p><b>Date :</b> {{ $date }}</p>
                
            </div>

            <div class="text-center">
                       
                    </div> 

            <div class="text-right">
                <!-- Display the meal -->
                
                <p><b>Submitted by :</b> {{ $submitter }}</p>
            </div> 
        </div>

        <!-- Update the table header to include columns for "Submitter" and "Meal" -->
        <div class="table-container">
            <table>
                <thead style="background: #4B8B5A; color: white;">
                    <tr>
                        <th style="text-align: center;">Sl No.</th>
                        <th>Item Name</th>
                        <th style="text-align: center;">Quantity Spoiled</th>
                        <th style="text-align: center;">Unit</th>
                        <!-- <th style="text-align: center;">ACtion</th> -->
                        
                    </tr>
                </thead>
                @foreach($vendorSpoils as $spoil)
                    @if(is_object($spoil))
                        <tr>

                            <td style="text-align: center;">{{ $spoil->sl_no }}</td>
                            <td>{{ $spoil->item_name }}</td>
                            <td style="text-align: center;">{{ $spoil->quantity }}</td>
                            <td style="text-align: center;">{{ $spoil->unit }}</td>
                        
                        </tr>
                    @endif
                @endforeach
            </table>
                        
        </div>

        @endforeach

       
    </div>
  </div>

  <script>

              
  var rowCount = 1; // Initialize a counter for Sl No

  function addRow() {
      var table = document.getElementById("dynamic-table");
      var newRow = table.insertRow(-1); // Insert at the end (after the last row)

      // Increment the counter for Sl No and set it in the new row's input field
      rowCount++;

      // Create the select element for Product Name
      var productSelect = document.createElement("select");
      productSelect.name = "item_name[]";
      productSelect.className = "form-control";
      productSelect.required = true;

      // Create the default option for Product Name
      var defaultProductOption = document.createElement("option");
      defaultProductOption.value = "";
      defaultProductOption.text = "--Select Product Name--";
      productSelect.appendChild(defaultProductOption);

      // Fetch and populate unique item names
      fetchUniqueItemNames(productSelect);

      // Create the select element for Unit
      var unitSelect = document.createElement("select");
      unitSelect.name = "unit[]";
      unitSelect.className = "form-control";
      unitSelect.required = true;

      // Create the default option for Unit
      var defaultUnitOption = document.createElement("option");
      defaultUnitOption.value = "kg";
      defaultUnitOption.text = "kg";
      unitSelect.appendChild(defaultUnitOption);

      // Fetch and populate unique units, excluding those in the static list
      fetchUniqueUnits(unitSelect);

      newRow.innerHTML = '<td><input type="number" name="sl_no[]" id="sl-no-' + rowCount + '" class="sl-no-field" value="' + rowCount + '" required /></td>' +
          '<td></td>' + // Leave an empty td for the product select
          '<td><input type="number" name="quantity[]" required /></td>' +
          '<td></td>' + // Leave an empty td for the unit select
          '<td style="font-size:24px; text-align:center"><button style="color:green" class="action-button" type="button" onclick="addRow()">+</button><button class="action-button" type="button" onclick="deleteRow(this)">-</button></td>';

      // Set the select elements in the new row
      newRow.cells[1].appendChild(productSelect);
      newRow.cells[3].appendChild(unitSelect);

      // Update the "Sl No" values for all rows
      updateSlNoValues();
  }

      function fetchUniqueUnits(unitSelect) {
      // Make an AJAX request to your Laravel route to fetch unique unit values
      fetch('/api/getUniqueUnits')
          .then((response) => response.json())
          .then((data) => {
              // Populate the unitSelect dropdown with unique unit values
              data.forEach((unitValue) => {
                  var unitOption = document.createElement("option");
                  unitOption.value = unitValue;
                  unitOption.text = unitValue;
                  unitSelect.appendChild(unitOption);
              });
          })
          .catch((error) => {
              console.error('Error fetching data:', error);
          });
  }

  // Function to fetch unique item names from the server and populate the product select dropdown
  function fetchUniqueItemNames(productSelect) {
      // Make an AJAX request to your Laravel route to fetch unique item names
      fetch('/api/getUniqueItemNames')
          .then((response) => response.json())
          .then((data) => {
              // Populate the productSelect dropdown with unique item names
              data.forEach((itemValue) => {
                  var itemOption = document.createElement("option");
                  itemOption.value = itemValue;
                  itemOption.text = itemValue;
                  productSelect.appendChild(itemOption);
              });
          })
          .catch((error) => {
              console.error('Error fetching data:', error);
          });
  }


  // Helper function to generate product options for the select element
  function getProductOptions(options) {
      var optionHTML = '';
      for (var i = 0; i < options.length; i++) {
          optionHTML += '<option value="' + options[i].value + '">' + options[i].text + '</option>';
      }
      return optionHTML;
  }

  function deleteRow(button) {
      var table = document.getElementById("dynamic-table");
      var row = button.parentNode.parentNode;
      table.deleteRow(row.rowIndex);

      // Update the "Sl No" values for all rows after deleting a row
      updateSlNoValues();
  }

  function updateSlNoValues() {
      var slNoFields = document.getElementsByClassName("sl-no-field");
      for (var i = 0; i < slNoFields.length; i++) {
          slNoFields[i].value = i + 1;
      }
  }

  </script>

</body>
</html>

