<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Issue;
use App\Models\Spoil;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class BalanceHistoryController extends Controller
{
    public function showDashboard()
    {
        // Initialize an array to store data for the past 11 months
        $historicalData = [];

        // Loop through the past 11 months
        for ($i = 0; $i < 11; $i++) {
            // Calculate the month and year for each iteration
            $targetMonth = Carbon::now()->subMonths($i)->format('m');
            $targetYear = Carbon::now()->subMonths($i)->format('Y');

            // Retrieve purchased items for the current iteration
            $purchasedItems = Product::select('item_name', 'quantity', 'unit')
                ->whereMonth('created_at', $targetMonth)
                ->whereYear('created_at', $targetYear)
                ->get();

            // Retrieve issue items for the current iteration
            $issueItems = Issue::select('item_name', 'quantity')
                ->whereMonth('created_at', $targetMonth)
                ->whereYear('created_at', $targetYear)
                ->get();

            // Retrieve spoiled items for the current iteration
            $spoiledItems = Spoil::select('item_name', 'quantity')
                ->whereMonth('created_at', $targetMonth)
                ->whereYear('created_at', $targetYear)
                ->get();

            // Create an associative array to store the combined data for each item name
            $combinedItems = [];

            // Process purchased items
            foreach ($purchasedItems as $purchasedItem) {
                $itemName = $purchasedItem->item_name;
                $quantity = $purchasedItem->quantity;

                // Initialize combined data for the item name
                if (!isset($combinedItems[$itemName])) {
                    $combinedItems[$itemName] = [
                        'item_name' => $itemName,
                        'total_quantity' => 0,
                        'unit' => $purchasedItem->unit,
                        'issue_quantity' => 0, // Initialize issue_quantity to 0
                        'spoiled_quantity' => 0, // Initialize spoiled_quantity to 0
                    ];
                }

                // Update the total quantity for the item name
                $combinedItems[$itemName]['total_quantity'] += $quantity;
            }

            // Process issue items
            foreach ($issueItems as $issueItem) {
                $itemName = $issueItem->item_name;
                $quantity = $issueItem->quantity;

                // Check if the item name exists in combinedItems
                if (isset($combinedItems[$itemName])) {
                    // Update the issue quantity for the item name
                    $combinedItems[$itemName]['issue_quantity'] += $quantity;
                }
            }

            // Process spoiled items
            foreach ($spoiledItems as $spoiledItem) {
                $itemName = $spoiledItem->item_name;
                $quantity = $spoiledItem->quantity;

                // Check if the item name exists in combinedItems
                if (isset($combinedItems[$itemName])) {
                    // Update the spoiled quantity for the item name
                    $combinedItems[$itemName]['spoiled_quantity'] += $quantity;
                }
            }

            // Calculate the balance for each item and store it in the item data
            foreach ($combinedItems as &$item) {
                $totalQuantity = $item['total_quantity'];
                $issueQuantity = $item['issue_quantity'];
                $spoiledQuantity = $item['spoiled_quantity'];
                $balance = $totalQuantity - ($issueQuantity + $spoiledQuantity);
                $item['balance'] = $balance;
            }

            // Get the previous month and year
            $previousMonth = Carbon::now()->subMonths($i + 1)->format('m');
            $previousYear = Carbon::now()->subMonths($i + 1)->format('Y');

            // Retrieve purchased items for the previous month
            $purchasedItems = Product::select('item_name', 'quantity', 'unit')
                ->whereMonth('created_at', $previousMonth)
                ->whereYear('created_at', $previousYear)
                ->get();

            // Retrieve issue items for the previous month
            $issueItems = Issue::select('item_name', 'quantity')
                ->whereMonth('created_at', $previousMonth)
                ->whereYear('created_at', $previousYear)
                ->get();

            // Retrieve spoiled items for the previous month
            $spoiledItems = Spoil::select('item_name', 'quantity')
                ->whereMonth('created_at', $previousMonth)
                ->whereYear('created_at', $previousYear)
                ->get();

            // Create an associative array to store the combined data for each item name
            $previousBalance = [];

            // Process purchased items
            foreach ($purchasedItems as $purchasedItem) {
                $itemName = $purchasedItem->item_name;
                $quantity = $purchasedItem->quantity;

                // Initialize combined data for the item name
                if (!isset($previousBalance[$itemName])) {
                    $previousBalance[$itemName] = [
                        'item_name' => $itemName,
                        'total_quantity' => 0,
                        'unit' => $purchasedItem->unit,
                        'issue_quantity' => 0, // Initialize issue_quantity to 0
                        'spoiled_quantity' => 0, // Initialize spoiled_quantity to 0
                    ];
                }

                // Update the total quantity for the item name
                $previousBalance[$itemName]['total_quantity'] += $quantity;
            }

            // Process issue items
            foreach ($issueItems as $issueItem) {
                $itemName = $issueItem->item_name;
                $quantity = $issueItem->quantity;

                // Check if the item name exists in previousBalance
                if (isset($previousBalance[$itemName])) {
                    // Update the issue quantity for the item name
                    $previousBalance[$itemName]['issue_quantity'] += $quantity;
                }
            }

            // Process spoiled items
            foreach ($spoiledItems as $spoiledItem) {
                $itemName = $spoiledItem->item_name;
                $quantity = $spoiledItem->quantity;

                // Check if the item name exists in previousBalance
                if (isset($previousBalance[$itemName])) {
                    // Update the spoiled quantity for the item name
                    $previousBalance[$itemName]['spoiled_quantity'] += $quantity;
                }
            }

            // Calculate the balance for each item and store it in the item data
            foreach ($previousBalance as &$item) {
                $totalQuantity = $item['total_quantity'];
                $issueQuantity = $item['issue_quantity'];
                $spoiledQuantity = $item['spoiled_quantity'];
                $balance = $totalQuantity - ($issueQuantity + $spoiledQuantity);
                $item['balance'] = $balance;
            }

            // Store the processed data for the current iteration in the historicalData array
            $historicalData[] = [
                'month' => Carbon::createFromDate($targetYear, $targetMonth)->format('M Y'),
                'combinedItems' => $combinedItems,
                'previousBalance' => $previousBalance,
            ];
        }

        return view('Balancehistory', compact('historicalData'));
    }
}

