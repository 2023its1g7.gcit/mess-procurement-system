<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Issue;

class SearchController extends Controller
{

public function index(Request $request)
{
    $issues = Issue::orderBy('created_at', 'desc');

    // Check if a date is provided in the request
    if ($request->has('date')) {
        $selectedDate = $request->input('date');
        $issues->whereDate('created_at', Carbon::parse($selectedDate)->format('Y-m-d'));
    }

    $groupedIssues = $issues->get()->groupBy(function ($issue) {
        return $issue->created_at->format('d/m/y h:i:s A');
    });

    return view('issuelist', ['groupedIssues' => $groupedIssues]);
}  
}
