<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Spoil;

class SpoiledController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $spoils = Spoil::orderBy('created_at', 'desc')->get();
        $groupedspoils = $spoils->groupBy(function ($spoil) {
            return $spoil->created_at->format('d/m/y h:i:s A');
        });
        
        
        return view('spoileditem', ['groupedspoils' => $groupedspoils]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('addspoil');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'sl_no.*' => 'required|numeric',
            'item_name.*' => 'required',
            'quantity.*' => 'required|numeric',
            'unit.*' => 'required',
        ]);

        // Extract the "Submitter" and "Meal" fields
        $submitter = $request->input('submitter')[0];
        

        // Process and insert the data into the database
        foreach ($request->input('sl_no') as $key => $slNo) {
            Spoil::create([
                'sl_no' => $slNo,
                'item_name' => $request->input('item_name')[$key],
                'quantity' => $request->input('quantity')[$key],
                'unit' => $request->input('unit')[$key],
                'submitter' => $submitter, // Use the same submitter for all rows
            ]);
        }

        // Redirect to the product index page or return a response as needed
        return redirect(route('spoillist'));
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
