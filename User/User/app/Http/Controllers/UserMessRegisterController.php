<?php

namespace App\Http\Controllers;

use App\Models\Messuser;
use Illuminate\Http\Request;
use App\Http\Requests\UserMessRegisterRequest;

class UserMessRegisterController extends Controller
{
    /**
     * Display register page.
     * 
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('auth.usermessregister');
    }

    /**
     * Handle account registration request
     * 
     * @param UserMessRegisterRequest $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function usermessregister(UserMessRegisterRequest $request) 
    {
        $messusers = Messuser::create($request->validated());

        auth()->login($messusers);

        return redirect('/')->with('success', "Account successfully registered.");
    }
}
