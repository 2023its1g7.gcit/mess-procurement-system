<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ForceLogout
{
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::check()) {
            // User is not authenticated, invalidate the session
            $request->session()->invalidate();
            $request->session()->regenerateToken();
        }

        return $next($request);
    }
}
