<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;  // Add this line
use Illuminate\Support\Collection;
use App\Models\Issue;

class IssuesExport implements FromCollection, WithHeadings  // Add WithHeadings here
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        // Fetch your issues data and transform it as needed
        $issues = Issue::all();

        // Map the issues to an array
        $exportData = $issues->map(function ($issue) {
            return [
                'Sl No.' => $issue->sl_no,
                'Item Name' => $issue->item_name,
                'Quantity' => $issue->quantity,
                'Unit' => $issue->unit,
                // Add more columns as needed
            ];
        });

        return $exportData;
    }
}
