<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category'; // Add this to your Category model if necessary
    protected $primaryKey = 'id';
    protected $fillable = ['item_name', 'unit','threshold'];
    use HasFactory;
}
