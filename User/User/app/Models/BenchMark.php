<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BenchMark extends Model
{
    protected $table = 'benchmark'; // Add this to your Category model if necessary
    protected $primaryKey = 'id';
    protected $fillable = ['item_name', 'unit','benchmark',];
    
    use HasFactory;
}
