<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Exports\IssuesExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportIssues extends Command
{
    protected $signature = 'export:issues';

    protected $description = 'Export issues to Excel file';

    public function handle()
    {
        return Excel::download(new IssuesExport, 'issues.xlsx');
    }
}
