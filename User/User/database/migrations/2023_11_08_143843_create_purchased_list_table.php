<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('purchased_list', function (Blueprint $table) {
            $table->id("purchased_id");
            $table->string('sl_no');
            $table->string('vendor');
            $table->string('item_name');
            $table->decimal('quantity', 10, 2);
            $table->string('unit');
            $table->integer('rate');
            $table->integer('amount');
            $table->string('cheque_no');
            $table->string('amt');
            $table->string('submitter');
            $table->string('image_path')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('purchased_list');
    }
};
