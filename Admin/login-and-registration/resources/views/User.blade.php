
<!DOCTYPE html>
<html lang="en">

<head>

  <title>GCIT Mess</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user.css') }}">

  <style>
        
    .custom-modal {
        display: none;
        position: fixed;
        z-index: 1;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: rgba(0, 0, 0, 0.4);
    }

    .modal-content {
        background-color: #fefefe;
        margin: 15% auto;
        padding: 20px;
        border: 1px solid #888;
        width: 110%;
        height: 18%;
        max-width: 400px;
        text-align: center;
        display: flex;
        flex-direction: column;
        justify-content: space-between; /* This will align buttons vertically */
    }

    .close {
        color: #aaa;
        margin-left:300px;
        font-size: 28px;
        font-weight: bold;
        cursor: pointer;
    }

    .close:hover,
    .close:focus {
        color: black;
        text-decoration: none;
    }

    .modal-buttons {
        display: flex;
        justify-content: space-between; /* This will align buttons horizontally */
        margin-top: 20px;
    }

    .cancel-btn {
        background-color: #ccc;
        color: #000;
        padding: 10px 20px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    .cancel-btn:hover {
        background-color: #ddd;
    }

    .delete-btn {
        background-color: #f44336;
        color: #fff;
        padding: 10px 20px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    .delete-btn:hover {
        background-color: #d32f2f;
    }

    #success-message {
        background-color: #4CAF50;
        color: white;
        padding: 10px;
        text-align: center;
        position: fixed;
        top: 20px;
        left: 50%;
        transform: translateX(-50%);
        z-index: 999;
        opacity: 1;
        transition: transform 10s ease-in-out, opacity 10s ease-in-out; /* Apply a 1-second slide and fade effect */
    }

    /* Add a class for sliding to the left */
    .slide-left {
        transform: translateX(-150%);
    }

    /* Add a class for sliding to the right */
    .slide-right {
        transform: translateX(150%);
    }

    /* Style for the modal */
    .modal {
      display: none; /* Hidden by default */
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.7); /* Semi-transparent background */
      z-index: 1;
      justify-content: center;
      align-items: center;
    }

    .modal-content {
      background-color: #fff;
      padding: 20px;
      border-radius: 5px;
      box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
      text-align: center;
    }

    /* Style for the close button */
    .close {
      position: absolute;
      top: 10px;
      right: 10px;
      font-size: 20px;
      cursor: pointer;
    }
    
    .custom-button {
        background-color:#4B8B5A;
        color:white;
        border: 1px solid #4B8B5A;
        padding: 10px 20px;
        text-decoration: none;
        display: inline-block;
        transition: all 0.3s ease-in-out;
        cursor: pointer;
    }

    .custom-button:hover {
        background-color: white;
        color: #4B8B5A;
        border-color:#4B8B5A;
    }
</style>

</head>

<body>
  @include('layouts.partials.navbar')
  <br>
  
  <div class="container-fluid">
    <div class="row content">

      <div class="col-sm-9">
          <div  class="topbar">
              <h4>Mess Coordinators</h4>   
          </div>

            <!-- Display "Submitter" and "Meal" fields above the loop -->
            <div class="text-container">
                <div class="text-left">
                    <!-- Display the submitter -->
                 
                </div>

                <div class="text-center">
                    
                </div>

                <div class="text-right" style="margin-bottom:15px">
                    <!-- Display the meal -->
                    <a href="/usermessregister"> 
                      <button class="text-bt">
                          <i class="fa fa-plus"></i> Add users
                      </button>
                    </a>
                    
                </div>
            </div>

            <!-- Update the table header to include columns for "Submitter" and "Meal" -->
            <div class="table-container">
                <table>
                    <thead style="background: #4B8B5A; color: white;">
                      <tr>
                        <th style="text-align: center;">SL.NO</th>
                        <th style="text-align: center;">Name</th>
                        <th style="text-align: center;">Student ID</th>
                        <th style="text-align: center;">Email Address</th>
                       
                        <th style="text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    @foreach($users as $user)
                    <tr>
                        <td style="text-align: center;">
                            {{ $loop->iteration }} {{-- Use loop iteration as the custom index starting from 1 --}}
                        </td>
                        <td style="text-align: center;">{{ $user->name }}</td>
                        <td style="text-align: center;">{{ $user->username }}</td>
                        <td style="text-align: center;">{{ $user->email }}</td>
                        
                        <td style="text-align: center;">
                            <button type="button" class="btn btn-danger" onclick="showConfirmationModal({{ $loop->iteration }})">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                            <div class="custom-modal" id="confirmation-modal-{{ $loop->iteration }}">
                                <div class="modal-content">
                                    <span class="close" onclick="hideConfirmationModal({{ $loop->iteration }})">&times;</span>
                                    <p>Are you sure you want to delete user {{ $user->name }}?</p>
                                    <div class="modal-buttons">
                                        <form action="{{ route('users.destroy', $user->id) }}" method="POST" onsubmit="showSuccessMessage()">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="delete-btn">Delete</button>
                                        </form>
                                        <button class="cancel-btn" onclick="hideConfirmationModal({{ $loop->iteration }})">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <div id="success-message-{{ $loop->iteration }}" class="alert alert-success" style="display: none;">
                        User {{ $user->name }} has been successfully deleted.
                    </div>
                @endforeach


                  </tbody>
          </table>
        </div>
    </div>
  </div>

  <script>
    //javascript for add user model

    // Get references to the modal and button
        var modal = document.getElementById("myModal");
        var openModalButton = document.getElementById("openModalButton");
        var closeModal = document.getElementById("closeModal");

        // Function to open the modal
        openModalButton.addEventListener("click", function() {
        modal.style.display = "block";
        });

        // Function to close the modal
        closeModal.addEventListener("click", function() {
        modal.style.display = "none";
        });

        // Close the modal if the user clicks outside of it
        window.addEventListener("click", function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
        });

  </script>
    <script>
        function showConfirmationModal(userId) {
    var modal = document.getElementById('confirmation-modal-' + userId);
    modal.style.display = 'block';
}

function hideConfirmationModal(userId) {
    var modal = document.getElementById('confirmation-modal-' + userId);
    modal.style.display = 'none';
}

    </script>

    <script>
 // JavaScript to show and slide out the success message
function showSuccessMessage() {
    var successMessage = document.getElementById('success-message');
    successMessage.style.display = 'block';

    // Apply the slide-left class after a delay
    setTimeout(function() {
        successMessage.classList.add('slide-left');
    }, 100);

    // Hide the success message after the slide-out effect completes
    setTimeout(function() {
        successMessage.style.display = 'none';
        successMessage.classList.remove('slide-left');
        successMessage.style.opacity = '1'; // Reset opacity for future use
    },5000); // 1100 milliseconds (1.1 seconds)
}


    </script>

    <script>
        // Get references to the modal and buttons
var modal = document.getElementById("myModal");
var showModalBtn = document.getElementById("showModalBtn");
var closeModalBtn = document.getElementById("closeModalBtn");

// Function to open the modal
showModalBtn.onclick = function() {
  modal.style.display = "block";
}

// Function to close the modal
closeModalBtn.onclick = function() {
  modal.style.display = "none";
}

// Close the modal if the user clicks outside of it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

    </script>

</body>
</html>

