<!DOCTYPE html>
<html lang="en">

<head>

  <title>GCIT Mess</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user.css') }}">

</head>

<body>
  @include('layouts.partials.navbar')
  <div class="container-fluid">
    <div class="row content">

      <div class="col-sm-9">
          <div  class="topbar">
              <h4>Add Purchased List</h4>   
          </div>

          <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data" >
                @csrf
                @method('post')

                <div class="text-container" style="margin-bottom:10px; margin-top:10px">
              <div class="text-left">
                  <select name="submitter[]" class="form-control" required>
                      <option value="">--Select submitter--</option>
                      <option value="Sonam Wangchuk">Sonam Wangchuk</option>
                      <option value="Tenzin Tshomo">Tenzin Tshomo</option>
                      <option value="Sonam Yangki">Sonam Yangki</option>
                  </select>
              </div>

              <div class="text-right">
              
                <button class="text-bt" data-toggle="modal" data-target="#flipFlop">
                    <i class="fa fa-plus"></i> Add category
                </button>
              
            </div>
              </div>

              
                <div class="table-container">
                    <table id="dynamic-table">
                        <thead style="background: #4B8B5A; color: white;">
                        <tr>
                            <th>Sl No.</th>
                            <th>Vendor</th>   
                            <th>Item Name</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Rate</th>
                            <th>Amount</th>
                            <th>Cheque No.</th>
                            <th>Amt</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        
                        <tr>
                            <td><input type="number" name="sl_no[]" id="sl-no-1" class="sl-no-field" value="1" required /></td>
                            
                            <td><input type="text" name="vendor[]" required /> </td>
                            
                            <td>
                                <select name="item_name[]" class="form-control" required>
                                    <option value="">--Select Product Name--</option>
                                    <option >Potato</option>
                                    <option >Tomato</option>
                                    <option >Onion</option>
                                    <option >Chilli</option>
                                    <option>Mushroom</option>
                                    @foreach ($categories->unique('item_name') as $category)
                                    @if (!in_array($category->item_name, ['Potato', 'Tomato', 'Onion', 'Chilli', 'Mushroom']))
                                        <option value="{{ $category->item_name }}">{{ $category->item_name }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </td>

                            <td><input type="number" name="quantity[]" class="calculate-amount" required /></td>

                            <td>
                                <select name="unit[]" class="form-control" required>
                                    <!-- <option value="">--Select Unit--</option> -->
                                    <option value="kg">kg</option>
                                    <option value="gram">gram</option>
                                    @foreach ($categories->unique('unit') as $category)
                                    @if (!in_array($category->unit, ['kg', 'gram', 'bunch']))
                                        <option value="{{ $category->unit }}">{{ $category->unit }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </td>
                            
                            <td><input type="number" name="rate[]" class="calculate-amount" required /></td>
                            <td><input type="number" name="amount[]" required /></td>

                            <td><input type="text" name="cheque_no[]" required /></td>
                            <td><input type="number" name="amt[]" required /></td>
                            <td>
                                <button class="action-button" type="button" onclick="addRow()">Add row</button>
                            </td>
                        </tr>

                    </table>
                    
                </div>
                
                <div class="text-container">
                <div class="text-left">

                    <input type="file" id="fileInput" name="images[]" accept=".pdf" multiple required style="display: none;" />
                    <label style="padding: 10px 0 0 8px;font-weight: normal;" for="fileInput" class="custom-file-button reciept"><span>Upload Receipt</span></label>

                    <div id="validationMessage" style="display: none; color: red;"></div>
                    <div id="selectedFile" style="display: none;"></div>
                </div>

                <div class="text-right">
                    <div class="row" style="margin-right:3px">
                        <input class="reciept" type="submit" value="Submit Data" />
                    </div>
                </div>
            </div>
            </form>

            <!-- The modal -->
            <div class="modal fade" id="flipFlop" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalLabel">Add Category</h4>
                    </div>

                    <div class="modal-body">
                    <form action="{{ route('storecategory') }}" method="post">
                        @csrf
                        @method('post')
                    
                        <input style="Height:40px; border-radius:5px; border:1px solid #73AE64" type="text" name="item_name" placeholder="Item Name" required>
                        <br><br>
                        
                        <input style="Height:40px; border-radius:5px; border:1px solid #73AE64" type="text" name="unit" placeholder="Unit" required>
                        <br><br>

                        <input style="Height:40px; border-radius:5px; border:1px solid #73AE64" type="number" name="threshold" placeholder="Minimum Stock Level" required>
                        <br><br>
                        
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </form>

                    </div>
                </div>
            </div>

            <script>

               // Get all input fields with class "calculate-amount" and calculate amount auto
               var inputFields = document.querySelectorAll('.calculate-amount');

                // Add event listeners to the input fields
                inputFields.forEach(function (inputField) {
                    inputField.addEventListener('input', function () {
                        // Get the quantity and rate values
                        var quantity = parseFloat(document.querySelector('input[name="quantity[]"]').value) || 0;
                        var rate = parseFloat(document.querySelector('input[name="rate[]"]').value) || 0;

                        // Calculate the amount
                        var amount = quantity * rate;

                        // Update the amount input field
                        document.querySelector('input[name="amount[]"]').value = amount;
                    });
                });

                // get file path
                document.getElementById('fileInput').addEventListener('change', function () {
                    var selectedFile = document.getElementById('selectedFile');
                    var validationMessage = document.getElementById('validationMessage');
                    var fileInput = this;

                    if (fileInput.files.length > 0) {
                        selectedFile.style.display = 'inline';
                        selectedFile.textContent = 'Selected File: ' + fileInput.files[0].name;
                        validationMessage.style.display = 'none';
                    } else {
                        selectedFile.style.display = 'none';
                        selectedFile.textContent = '';
                    }
                });

                function validateForm() {
                    var fileInput = document.getElementById('fileInput');
                    var validationMessage = document.getElementById('validationMessage');

                    if (fileInput.files.length === 0) {
                        validationMessage.style.display = 'inline';
                        validationMessage.textContent = 'Please select a file before submitting the form.';
                        return false; // Prevent form submission
                    }
                    return true; // Continue with form submission
                }

                function calculateAmount(input) {
                // Find the row containing the input element
                var row = input.closest("tr");

                // Get the quantity and rate values from the current row
                var quantity = parseFloat(row.querySelector('input[name="quantity[]"]').value) || 0;
                var rate = parseFloat(row.querySelector('input[name="rate[]"]').value) || 0;

                // Calculate the amount
                var amount = quantity * rate;

                // Update the amount input field in the current row
                row.querySelector('input[name="amount[]"]').value = amount;
            }

            var rowCount = 1; // Initialize a counter for Sl No

            function addRow() {
                var table = document.getElementById("dynamic-table");
                var newRow = table.insertRow(-1); // Insert at the end (after the last row)

                // Increment the counter for Sl No and set it in the new row's input field
                rowCount++;

                // Create the select element for Product Name
                var productSelect = document.createElement("select");
                productSelect.name = "item_name[]";
                productSelect.className = "form-control";
                productSelect.required = true;

                // Create the default option for Product Name
                var defaultProductOption = document.createElement("option");
                defaultProductOption.value = "";
                defaultProductOption.text = "--Select Product Name--";
                productSelect.appendChild(defaultProductOption);

                // Fetch and populate unique item names
                fetchUniqueItemNames(productSelect);

                // Create the select element for Unit
                var unitSelect = document.createElement("select");
                unitSelect.name = "unit[]";
                unitSelect.className = "form-control";
                unitSelect.required = true;

                // Create the default option for Unit
                var defaultUnitOption = document.createElement("option");
                defaultUnitOption.value = "kg";
                defaultUnitOption.text = "kg";
                unitSelect.appendChild(defaultUnitOption);

                // Fetch and populate unique units, excluding those in the static list
                fetchUniqueUnits(unitSelect);

                newRow.innerHTML = 

                    '<td><input type="number" name="sl_no[]" id="sl-no-' + rowCount + '" class="sl-no-field" value="' + rowCount + '" required /></td>' +
                                '<td><input type="text" name="vendor[]" required /></td>' +
                                '<td></td>' + // Leave an empty td for the product select
                                '<td><input type="number" name="quantity[]" required oninput="calculateAmount(this)" /></td>' + // Added oninput event
                                '<td></td>' + // Leave an empty td for the product select
                                '<td><input type="number" name="rate[]" required oninput="calculateAmount(this)" /></td>' + // Added oninput event
                                '<td><input type="number" name="amount[]" required /></td>' +
                                '<td><input type="text" name="cheque_no[]" required /></td>' +
                                '<td><input type="number" name="amt[]" required /></td>' +
                                '<td style="font-size:24px"><button style="color:green" class="action-button" type="button" onclick="addRow()">+</button><button class="action-button" type="button" onclick="deleteRow(this)">-</button></td>';

                // Set the select elements in the new row
                newRow.cells[2].appendChild(productSelect);
                newRow.cells[4].appendChild(unitSelect);

                // Update the "Sl No" values for all rows
                updateSlNoValues();
            }

                function fetchUniqueUnits(unitSelect) {
                // Make an AJAX request to your Laravel route to fetch unique unit values
                fetch('/api/getUniqueUnits')
                    .then((response) => response.json())
                    .then((data) => {
                        // Populate the unitSelect dropdown with unique unit values
                        data.forEach((unitValue) => {
                            var unitOption = document.createElement("option");
                            unitOption.value = unitValue;
                            unitOption.text = unitValue;
                            unitSelect.appendChild(unitOption);
                        });
                    })
                    .catch((error) => {
                        console.error('Error fetching data:', error);
                    });
            }

            // Function to fetch unique item names from the server and populate the product select dropdown
            function fetchUniqueItemNames(productSelect) {
                // Make an AJAX request to your Laravel route to fetch unique item names
                fetch('/api/getUniqueItemNames')
                    .then((response) => response.json())
                    .then((data) => {
                        // Populate the productSelect dropdown with unique item names
                        data.forEach((itemValue) => {
                            var itemOption = document.createElement("option");
                            itemOption.value = itemValue;
                            itemOption.text = itemValue;
                            productSelect.appendChild(itemOption);
                        });
                    })
                    .catch((error) => {
                        console.error('Error fetching data:', error);
                    });
            }


            // Helper function to generate product options for the select element
            function getProductOptions(options) {
                var optionHTML = '';
                for (var i = 0; i < options.length; i++) {
                    optionHTML += '<option value="' + options[i].value + '">' + options[i].text + '</option>';
                }
                return optionHTML;
            }

            function deleteRow(button) {
                var table = document.getElementById("dynamic-table");
                var row = button.parentNode.parentNode;
                table.deleteRow(row.rowIndex);

                // Update the "Sl No" values for all rows after deleting a row
                updateSlNoValues();
            }

            function updateSlNoValues() {
                var slNoFields = document.getElementsByClassName("sl-no-field");
                for (var i = 0; i < slNoFields.length; i++) {
                    slNoFields[i].value = i + 1;
                }
            }
          </script>
    </div>
  </div>

</body>
</html>
