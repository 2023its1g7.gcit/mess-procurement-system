
<!DOCTYPE html>
<html lang="en">

<head>

  <title>GCIT Mess</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/xlsx/dist/xlsx.full.min.js"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">

  <style>
  .custom-modal-dialog {
      max-width: 50%;
      max-height: 100vh; /* Adjust the desired height in pixels */
  }

  </style>

</head>

<body>
@include('layouts.partials.navbar')
  <div class="container-fluid">
    <div class="row content">

      <div class="col-sm-9">
          <div  class="topbar">
              <h4>Purchased List</h4>   
              <div class="date-search" style="display: flex; align-items: center;">
              
              <form action="{{ route('product.index') }}" method="get">
                @csrf
                <div class="date-search" style="display: flex;">
                    <input type="date" class="form-control" name="date" id="datepicker"  value="{{ request('date') }}" style="border-top-right-radius: 0; border-bottom-right-radius: 0;">
                    <button type="submit" class="btn btn-primary" style="border-top-left-radius: 0; border-bottom-left-radius: 0;background-color:#4B8B5A;border:1px solid #4B8B5A">
                    <i class="fa fa-search"></i>
                    </button>
                </div>
            </form>
            </div>
          </div>

          
          @foreach($groupedProducts as $date => $vendorProducts)
          <br>
        
            @php
                
                $submitter = null;

                // Check if there are any vendor issues to fetch meal and submitter from
                if (count($vendorProducts) > 0 && is_object($vendorProducts[0])) {
                    $firstIssue = $vendorProducts[0];

                    // Fetch meal and submitter from the first issue
                 
                    $submitter = $firstIssue->submitter;
                }
            @endphp
          <div class="text-container">
            <div class="text-left month-info">
                <!-- <p>Date : 02/10/2023</p> -->
                <p style="font-size:16px"><b>Date :</b>{{ $date }}</p>
                
            </div>

            <div class="text-center">
                <!-- <p>Line in the Middle</p> -->
                
            </div> 
            <div class="text-right">
                <!-- Display the meal -->
                
                <p><b>Submitted by :</b> {{ $submitter }}</p>
            </div>

          </div>
          
             <!-- Display the vendor's name as a heading -->
            <div class="table-container">
                <table>
                    <thead style="background: #4B8B5A; color: white;">
                        <tr>
                            <th style="text-align: center;">Sl No.</th>
                            <th>Vendor</th>
                            <th>Item Name</th>
                            <th style="text-align: center;">Quantity</th>
                            <th style="text-align: center;">Unit</th>
                            <th style="text-align: center;">Rate</th>
                            <th style="text-align: center;">Amount</th>
                            <th style="text-align: center;">Cheque No.</th>
                            <th style="text-align: center;">Paid Amount</th>
            
                        </tr>
                    </thead>
                    @foreach($vendorProducts as $product)
                        @if(is_object($product))
                            <tr>
                                <td style="text-align: center;">{{ $product->sl_no }}</td>
                                <td>{{ $product->vendor }}</td>
                                <td>{{ $product->item_name }}</td>
                                <td style="text-align: center;">{{ $product->quantity }}</td>
                                <td style="text-align: center;">{{ $product->unit }}</td>
                                <td style="text-align: center;">{{ $product->rate }}</td>
                                <td style="text-align: center;">{{ $product->amount }}</td>
                                <td style="text-align: center;">{{ $product->cheque_no }}</td>
                                <td style="text-align: center;">{{ $product->amt }}</td>
                                
                            </tr>
                        @endif
                    @endforeach
                </table>
                
                
            </div>

            <div class="text-container">
                <!-- empty -->
                </div>

                <div class="text-right" style="margin-right:10px; margin-top:13px">
                    
                    <button class="reciept view-image-btn" data-toggle="modal" data-target="#imageModal" data-image="{{ asset($vendorProducts[0]->image_path) }}">View Receipt</button>
                    <button class="reciept view-image-btn exportExcelBtn2" >Export Excel</button>

                </div>
        @endforeach

        <!-- Image Modal -->
        <div class="modal fade" style="z-index:99999;" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered custom-modal-dialog" role="document">
              <div class="modal-content ">
                  <div class="modal-header">
                      <h5 class="modal-title" id="imageModalLabel">Receipt Preview</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body" id="filePreview">
                  </div>
              </div>
          </div>
      </div>
  
    </div>
  </div>

  <script>
    document.addEventListener("DOMContentLoaded", function () {
        // Check if jQuery is loaded
        if (typeof jQuery === 'undefined') {
            console.error('jQuery is not loaded.');
        }

        // Attach click event listener to each "Export Excel" button
        $('.exportExcelBtn2').click(function () {
            // Get the index of the clicked button
            var index = $('.exportExcelBtn2').index(this);
            console.log("Export button clicked for table at index:", index);
            exportToExcel(index);
        });

        function exportToExcel(index) {
            // Get the month information
            var monthInfo = document.querySelectorAll(".month-info")[index].textContent.trim();

            // Create a new workbook
            var wb = XLSX.utils.book_new();

            // Extract data from the specific table
            var ws = XLSX.utils.table_to_sheet(document.querySelectorAll("table")[index]);

            // Add the worksheet to the workbook
            XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

            // Save the workbook as an Excel file with the month information in the name
            XLSX.writeFile(wb, `Purchased_Product_History_of_${monthInfo}.xlsx`);
            }
        });
    </script>

  <script>
    $(document).ready(function () {
        // Listen for the click event of the "View Image/Document" button
        $('.view-image-btn').click(function () {
            // Get the file path from the data-image attribute
            var filePath = $(this).data('image');
            console.log('File Path:', filePath); // Print the file path to the console for confirmation

            // Clear the previous content
            $('#filePreview').empty();

            // Extract the file extension from the file path
            var fileExtension = filePath.split('.').pop().toLowerCase();

            // Check the file extension to determine how to display it
            if (fileExtension === 'pdf') {
                // If it's a PDF, create an <iframe> to display it
                var iframe = $('<iframe>', {
                    src: filePath,
                    type: 'application/pdf',
                    style: 'width: 100%; height: 400px;'
                });
                $('#filePreview').append(iframe);
            } else if (fileExtension === 'doc' || fileExtension === 'docx') {
                // If it's a Word document, create an <iframe> to display it
                var iframe = $('<iframe>', {
                    src: 'https://view.officeapps.live.com/op/embed.aspx?src=' + filePath,
                    style: 'width: 100%; height: 400px;'
                });
                $('#filePreview').append(iframe);
            } else {
                // If it's an image, create an <img> element to display it
                var image = $('<img>', {
                    src: filePath,
                    alt: 'File Preview',
                    style: 'max-width: 100%;'
                });
                $('#filePreview').append(image);
            }
        });
    });
    </script>

</body>
</html>
