


    <!-- <div class="bg-light p-5 rounded">
        @auth
        <h1>Dashboard</h1>
        <p class="lead">Only authenticated users can access this section.</p>
        <a class="btn btn-lg btn-primary" href="https://codeanddeploy.com" role="button">View more tutorials here &raquo;</a>
        @endauth

        @guest
        <h1>Homepage</h1>
        <p class="lead">Your viewing the home page. Please login to view the restricted data.</p>
        @endguest
    </div> -->


    
    <!DOCTYPE html>
<html lang="en">

<head>

  <title>GCIT Mess</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link rel="stylesheet" type="text/css" href="./css/dashboard.css">

</head>

<body>
  <div class="container-fluid">
    <div class="row content">
    @include('layouts.partials.navbar')

   
      
      <div class="col-sm-9">
        
          <div  class="topbar">
              <h4>Dashboard</h4>

              <p style="color:black">
                @auth
                  {{auth()->user()->name}}
                @endauth
              </p>
              
              
          </div>

          <div class="col-sm-4" style="padding-top: 15px;">
                <div class="threebox">
                  <div style="display: flex; align-items: center;">
                    <img src="{{ asset('img/1.png') }}" alt="Image Alt Text" style="max-width: 50px; max-height: 50px; margin-right: 20px;margin-left: 20px;">
                      <div>
                        <p>Current Amount Spend</p>
                        <h4>Nu. 2000,000</h4>
                      </div>
                  </div>
                  </div>
              </div>
              <div class="col-sm-4" style="padding-top: 15px;">
                <div class="threebox">
                  <div style="display: flex; align-items: center;">
                      <img src="{{ asset('img/2.png') }}" alt="Image Alt Text" style="max-width: 50px; max-height: 50px; margin-right: 20px;margin-left: 20px;">
                      <div>
                        <p>Monthly Average Spent</p>
                        <h4>Nu. 195,000</h4>
                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="padding-top: 15px;">
                <div class="threebox">
                      <div style="display: flex; margin-left:30px">
                      
                          <div>

                              <p>Potato : <span></span></p>
                              <p>Tomato : <span></span></p>
                              <p>Cabbage : <span></span></p>
                              <p>Chilli : <span></span></p>
                              <p>Onion : <span></span></p>
                              
                          </div>
                      </div>
                  </div>
              </div>

              <div class="row">
                  <div class="col-sm-8">
                      <div style="border-radius: 10px;border: 1px solid rgba(0, 0, 0, 0.30);background: #FFF; margin:14px;height: auto;flex-shrink: 0; padding:15px">
                          <p>Customers Joined</p>
                          <canvas id="myChart" style="width:100%;max-width:600px"></canvas>

                          <script>
                              var xValues = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                              var yValues = [200000, 150000, 148000, 100000, 100032, 80000, 90000, 210000, 60000, 230000, 220000, 120000];
                              var barColors = "#6A97F9"; // Use a single color

                              new Chart("myChart", {
                                  type: "bar",
                                  data: {
                                      labels: xValues,
                                      datasets: [{
                                          backgroundColor: barColors,
                                          data: yValues
                                      }]
                                  },
                                  options: {
                                      legend: { display: false },
                                      title: {
                                          display: true,
                                          text: "Monthly Spend Money"
                                      },
                                      scales: {
                                          xAxes: [{
                                              scaleLabel: {
                                                  display: true,
                                                  labelString: 'Months' // X-axis label
                                              }
                                          }],
                                          yAxes: [{
                                              scaleLabel: {
                                                  display: true,
                                                  labelString: 'Amount (Nu)' // Y-axis label
                                              },
                                              ticks: {
                                                  beginAtZero: true // Start the y-axis at zero
                                              }
                                          }]
                                      }
                                  }
                              });
                          </script>
                      </div>
                  </div>
                  
              </div>

              <div class="col-sm-8">
                  <div style="border-radius: 10px; border: 1px solid rgba(0, 0, 0, 0.30); background: #FFF; margin:14px; height: auto; flex-shrink: 0; padding:15px">
                        
                      <script type="text/javascript">
                          window.onload = function () {
                              var chart = new CanvasJS.Chart("chartContainer",
                              {

                              title:{
                              text: "Amount Spent - per month"
                              },
                              axisX: {
                                  valueFormatString: "MMM",
                                  interval:1,
                                  intervalType: "month"
                              },
                              axisY:{
                                  includeZero: false

                              },
                              data: [
                              {
                                  type: "line",

                                  dataPoints: [
                                  { x: new Date(2012, 00, 1), y: 150000 },
                                  { x: new Date(2012, 01, 1), y: 210414},
                                  { x: new Date(2012, 02, 1), y: 200520},
                                  { x: new Date(2012, 03, 1), y: 210460 },
                                  { x: new Date(2012, 04, 1), y: 200450 },
                                  { x: new Date(2012, 05, 1), y: 230500 },
                                  { x: new Date(2012, 06, 1), y: 310480 , indexLabel: "highest",markerColor: "red", markerType: "triangle"},
                                  { x: new Date(2012, 07, 1), y: 210480 },
                                  { x: new Date(2012, 08, 1), y: 123410 , indexLabel: "lowest",markerColor: "DarkSlateGrey", markerType: "cross"},
                                  { x: new Date(2012, 09, 1), y: 213500 },
                                  { x: new Date(2012, 10, 1), y: 142480 },
                                  { x: new Date(2012, 11, 1), y: 241510 }
                                  ]
                              }
                              ]
                              });

                              chart.render();
                          }
                          </script>
                          <!-- <script type="text/javascript" src="https://cdn.canvasjs.com/canvasjs.min.js"></script> -->
                            
                          <div id="chartContainer" style="height: 300px; width: 100%;">
                          </div>
                              
                          
                          <script type="text/javascript" src="https://cdn.canvasjs.com/canvasjs.min.js"></script>
                  </div>
              </div>
      </div>
    </div>
  </div>

<script>
    // JavaScript
  const navLinks = ["dashboard-link", "product-link", "customers-link", "feedback-link"];

  navLinks.forEach(linkId => {
    const link = document.getElementById(linkId);
    link.addEventListener("click", function() {
      // Remove the "active" class from all list items
      navLinks.forEach(id => {
        const listItem = document.getElementById(id);
        listItem.classList.remove("active");
      });
      // Add the "active" class to the clicked link
      link.classList.add("active");
    });
  });
  // JavaScript
  const navLinks1 = ["dashboard-link1", "product-link1", "customers-link1", "feedback-link1"];

  navLinks1.forEach(linkId => {
    const link = document.getElementById(linkId);
    link.addEventListener("click", function() {
      // Remove the "active" class from all list items
      navLinks1.forEach(id => {
        const listItem = document.getElementById(id);
        listItem.classList.remove("active");
      });
      // Add the "active" class to the clicked link
      link.classList.add("active");
    });
  });

    function toggleDropdown(dropdown) {
      dropdown.classList.toggle("active");
  }
  </script>

</body>
</html>

