<!DOCTYPE html>
<html lang="en">

<head>

  <title>GCIT Mess</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <link rel="stylesheet" type="text/css" href="./css/dashboard.css">

  <style>
        /* Center-align text */
        .text-left {
            text-align: left;
            text-decoration: underline
        }

        /* Right-align text */
        .text-right {
            text-align: right;
        }

        /* Create a container for the text */
        .text-container {
            display: flex;
            justify-content: space-between;
            margin:0 15px;
            margin-bottom:-6px
        }
    </style>

</head>

<body>
@include('layouts.partials.navbar')
  <div class="container-fluid">
    <div class="row content">
      <br/>
      <div class="col-sm-9">
            <div class="topbar d-flex justify-content-between align-items-center">
            <h4>Purchased List</h4>
            <div class="date-search" style="display: flex; align-items: center;">
            <input type="date" class="form-control datepicker" id="datepicker" data-mdb-inline="true" value="2023-10-06" style="border-top-right-radius: 0; border-bottom-right-radius: 0;">
            <button class="btn btn-primary" style="border-top-left-radius: 0; border-bottom-left-radius: 0;background-color:#4B8B5A;border:1px solid #4B8B5A">
            <i class="fa fa-search"></i>
            </button>
            </div>

      </div>
      <br/>
      <br/>

      <div class="text-container">
            <div class="text-left">
                <p>Date : 02/10/2023</p>
            </div>

            <!-- <div class="text-center">
                <p>Line in the Middle</p>
            </div> -->

            <div class="text-right">
                <p>Submitted By : Sonam Wangchuk</p>
            </div>
        </div>


        <div class="row">
        <div class="col-sm-12">
        <div class="overflowx" style="border-radius: 10px; background: #FFF; margin-right: 30px;">
        <table style="border: 1px solid rgba(0, 0, 0, 0.30); margin: 14px; width: 100%;border-radius:5px" class="table table-bordered">
                  <thead style="background-color: #4B8B5A; color: white;">
                    <tr>
                      <th style="text-align: center;">SL.NO</th>
                      <th style="text-align: center;">Vendor</th>
                      <th style="text-align: center;">Item Name</th>
                      <th style="text-align: center;">Quantity</th>
                      <th style="text-align: center;">Unit</th>
                      <th style="text-align: center;">Rate</th>
                      <th style="text-align: center;">Amount</th>
                      <th style="text-align: center;">Cheque Number</th>
                      <th style="text-align: center;">Amt</th>

                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="text-align: center;">1</td>
                      <td style="text-align: center;">Tashi Tshongkhang</td>
                      <td style="text-align: center;"> Potato</td>
                      <td style="text-align: center;">5</td>
                      <td style="text-align: center;">kg</td>
                      <td style="text-align: center;">80</td>
                      <td style="text-align: center;">400</td>
                      <td style="text-align: center;">(19-oct) 00234</td>
                      <td style="text-align: center;">1500</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;">1</td>
                      <td style="text-align: center;">Tashi Tshongkhang</td>
                      <td style="text-align: center;"> Potato</td>
                      <td style="text-align: center;">5</td>
                      <td style="text-align: center;">kg</td>
                      <td style="text-align: center;">80</td>
                      <td style="text-align: center;">400</td>
                      <td style="text-align: center;">(19-oct) 00234</td>
                      <td style="text-align: center;">1500</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;">1</td>
                      <td style="text-align: center;">Tashi Tshongkhang</td>
                      <td style="text-align: center;"> Potato</td>
                      <td style="text-align: center;">5</td>
                      <td style="text-align: center;">kg</td>
                      <td style="text-align: center;">80</td>
                      <td style="text-align: center;">400</td>
                      <td style="text-align: center;">(19-oct) 00234</td>
                      <td style="text-align: center;">1500</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;">1</td>
                      <td style="text-align: center;">Tashi Tshongkhang</td>
                      <td style="text-align: center;"> Potato</td>
                      <td style="text-align: center;">5</td>
                      <td style="text-align: center;">kg</td>
                      <td style="text-align: center;">80</td>
                      <td style="text-align: center;">400</td>
                      <td style="text-align: center;">(19-oct) 00234</td>
                      <td style="text-align: center;">1500</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;">1</td>
                      <td style="text-align: center;">Tashi Tshongkhang</td>
                      <td style="text-align: center;"> Potato</td>
                      <td style="text-align: center;">5</td>
                      <td style="text-align: center;">kg</td>
                      <td style="text-align: center;">80</td>
                      <td style="text-align: center;">400</td>
                      <td style="text-align: center;">(19-oct) 00234</td>
                      <td style="text-align: center;">1500</td>
                    </tr>
                    
                  </tbody>
                </table>
                <button class="table-button" style="    box-shadow: 4px 6px 8px rgba(0, 0, 0, 0.1);  /* Horizontal offset, Vertical offset, Blur radius, Shadow color */
">View Reciept</button>

              </div>        
        </div>  
      </div>


    </div>
  </div>

 <script>
$(document).ready(function() {
    $('.datepicker').pickadate();
});
 </script> 
<script>
    // JavaScript
  const navLinks = ["dashboard-link", "product-link", "customers-link", "feedback-link"];

  navLinks.forEach(linkId => {
    const link = document.getElementById(linkId);
    link.addEventListener("click", function() {
      // Remove the "active" class from all list items
      navLinks.forEach(id => {
        const listItem = document.getElementById(id);
        listItem.classList.remove("active");
      });
      // Add the "active" class to the clicked link
      link.classList.add("active");
    });
  });
  // JavaScript
  const navLinks1 = ["dashboard-link1", "product-link1", "customers-link1", "feedback-link1"];

  navLinks1.forEach(linkId => {
    const link = document.getElementById(linkId);
    link.addEventListener("click", function() {
      // Remove the "active" class from all list items
      navLinks1.forEach(id => {
        const listItem = document.getElementById(id);
        listItem.classList.remove("active");
      });
      // Add the "active" class to the clicked link
      link.classList.add("active");
    });
  });

    function toggleDropdown(dropdown) {
      dropdown.classList.toggle("active");
  }
  
  </script>

</body>
</html>
