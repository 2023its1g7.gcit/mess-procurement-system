<!DOCTYPE html>
<html lang="en">

<head>

    <title>GCIT Mess</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,800;0,900;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/dashboard.css">

    <style>
        /* Center-align text */
        .text-left {
            text-align: left;
            text-decoration: underline
        }

        /* Right-align text */
        .text-right {
            text-align: right;
        }

        /* Create a container for the text */
        .text-container {
            display: flex;
            justify-content: space-between;
            margin: 0 15px;
            margin-bottom: -6px
        }
    </style>

</head>

<body>
    @include('layouts.partials.navbar')
    <br>

    <div class="container-fluid">
        <div class="row content">
            <div class="col-sm-9">
                <div class="topbar">
                    <h4> Balance Check</h4>
                    <!-- Search and Calendar Section -->

                </div>

                <br>

                @foreach ($historicalData as $data)
                    <div class="text-container">
                        <div class="text-left">
                        <p style="font-size: 16px;" id="currentDate"><b>{{ $data['month'] }}</b></p>
                        </div>

                        <div class="text-right">
                            <p>Balance Check for month : {{ $data['month'] }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="overflowx" style="border-radius: 10px; background: #FFF; margin-right: 30px;">

                                <table style="border: 1px solid rgba(0, 0, 0, 0.30); margin: 14px; width: 100%;"
                                    class="table table-bordered">
                                    <thead style="background-color: rgba(58, 104, 68, 0.90); color: white;">
                                        <tr>
                                            <th style="text-align: center;">SL.NO</th>
                                            <th style="text-align: center;">Item Name</th>
                                            <th style="text-align: center;">Balance brought forward</th>
                                            <th style="text-align: center;">Purchased Item Quantity </th>
                                            <th style="text-align: center;">Unit</th>
                                            <th style="text-align: center;">Total Quantity Item</th> 
                                            <th style="text-align: center;">Item Usage (Issued)</th>
                                            <th style="text-align: center;">Spoiled Item quantity</th>
                                            <th style="text-align: center;">Remaining Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data['previousBalance'] as $previousItem)
                                            @if (!isset($data['combinedItems'][$previousItem['item_name']]) && $previousItem['balance'] > 0)
                                                <tr>
                                                    <td style="text-align: center;">{{ $loop->index + 1 }}</td>
                                                    <td style="text-align: center;">{{ $previousItem['item_name'] }}</td>
                                                    <td style="text-align: center;">{{ $previousItem['balance'] }}</td>
                                                    
                                                    <td style="text-align: center;">0</td>
                                                    <td style="text-align: center;">{{ $previousItem['unit'] }}</td>
                                                    <td style="text-align: center;">{{ $previousItem['balance'] }}</td>
                                                   
                                                    <td style="text-align: center;">0</td>
                                                    <td style="text-align: center;">0</td>
                                                    <td style="text-align: center;">{{ $previousItem['balance'] }}</td>
                                                </tr>
                                            @endif
                                        @endforeach

                                        @foreach ($data['combinedItems'] as $item)
                                            <tr>
                                                <td style="text-align: center;">{{ $loop->index + 1 }}</td>
                                                <td style="text-align: center;">{{ $item['item_name'] }}</td>
                                                <td style="text-align: center;">
                                                
                                                    @if (isset($data['previousBalance'][$item['item_name']]))
                                                        {{ $data['previousBalance'][$item['item_name']]['balance'] }}
                                                    @else
                                                        0
                                                    @endif
                                                </td>
                                                <td style="text-align: center;">{{ $item['total_quantity'] }}</td>
                                                <td style="text-align: center;">{{ $item['unit'] }}</td>
                                                <td style="text-align: center;">
                                                    @php
                                                        $newTotal = $item['total_quantity'] + (isset($data['previousBalance'][$item['item_name']]) ? $data['previousBalance'][$item['item_name']]['balance'] : 0);
                                                    @endphp
                                                    {{ $newTotal }}
                                                </td>
                                                
                                                <td style="text-align: center;">{{ $item['issue_quantity'] }}</td>
                                                <td style="text-align: center;">{{ $item['spoiled_quantity'] }}</td>
                                                <td style="text-align: center;">
                                                    @php
                                                        $remainingBalance = $newTotal - $item['spoiled_quantity'] - $item['issue_quantity'];
                                                    @endphp
                                                    {{ $remainingBalance }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>


    <script>
        // Function to format the date as "dd/mm/yyyy"
        function formatDate(date) {
            const day = date.getDate().toString().padStart(2, '0');
            const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-based
            const year = date.getFullYear();
            return `${day}/${month}/${year}`;
        }

        // Get the element by its ID
        const currentDateElement = document.getElementById('currentDate');

        // Update the date content
        const currentDate = formatDate(new Date());
        currentDateElement.textContent = `Date: ${currentDate}`;
    </script>

</body>

</html>
