<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Loading...</title>
</head>
<body>
    <div class="loader"></div>

   
     <div id="myModal" class="modal">
        <div class="modal-content">
            <span class="close"></span>
            <p>Successfully Changed</p>
            <button class="w-100 btn"  id="okButton">OK</button>
        </div>
    </div> 

    <script>
        // Hide the modal initially
        var modal = document.getElementById("myModal");
        modal.style.display = "none";

        // Show the loader initially
        var loader = document.querySelector(".loader");
        loader.style.display = "block";

        // Close the modal if the user clicks the close button (×)
        var closeBtn = document.querySelector(".close");
        closeBtn.addEventListener("click", function () {
            modal.style.display = "none";
            window.location.href = '/';
        });

        // Close the modal if the user clicks the OK button
        var okButton = document.getElementById("okButton");
        okButton.addEventListener("click", function () {
            modal.style.display = "none";
            window.location.href = '/';
        });

        // Execute the showModal function after a delay
        setTimeout(showModal, 3000); // Adjust the delay in milliseconds (3 seconds in this example)

        // Show the modal with the success message
        function showModal() {
            // Hide the loader and show the modal after a delay
            loader.style.display = "none";
            modal.style.display = "block";

            // Close the modal after 10 seconds (adjust the delay as needed)
            setTimeout(function () {
                modal.style.display = "none";
                window.location.href = '/';
            }, 10000); // 10 seconds
        }
    </script>
</body>
</html>

<style>
    /* Add your CSS styles for the modal and loader here */
    /* ... */

    /* Style the modal */
    .modal {
        position: fixed;
        z-index: 1;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.7);
    }

    /* Modal content */
    .modal-content {
        background-color: #fff;
    margin: 10% auto;
    padding: 20px;
    border: 1px solid #ccc;
    border-radius: 5px;
    max-width: 400px; /* Adjust the maximum width as needed */
    width: 90%;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    text-align: center;
    font-size: 1.2rem;
    margin-bottom: 20px;
    font-family: 'Roboto', sans-serif;
    }
    .btn {
    background-color:#417A4E;
    color: #fff;
    height:30px;
    width:20%;
    border-color:white;
}

    /* Close button (×) */
    .close {
        color: #888;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }
    body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .loader {
            width: fit-content;
            font-weight: bold;
            font-family: monospace;
            font-size: 30px;
            background: radial-gradient(circle closest-side,  rgba(58, 104, 68, 0.90) 94%, #0000) right/calc(200% - 1em) 100%;
            animation: l24 1s infinite alternate linear;
        }

        .loader::before {
            content: "Loading...";
            line-height: 1em;
            color: #0000;
            background: inherit;
            background-image: radial-gradient(circle closest-side, #fff 94%, #000);
            -webkit-background-clip: text;
            background-clip: text;
        }

        @keyframes l24 {
            100% {
                background-position: left;
            }
        }
</style>
