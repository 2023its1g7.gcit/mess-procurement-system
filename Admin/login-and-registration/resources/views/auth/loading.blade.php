<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Loading...</title>
</head>
<body>
<div class="loader"></div>
    <script>
        setTimeout(function () {
            window.location.href = '/dashboard';
        }, 3000); // Adjust the delay in milliseconds (3 seconds in this example)
    </script>
</body>
</html>
<style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .loader {
            width: fit-content;
            font-weight: bold;
            font-family: monospace;
            font-size: 30px;
            background: radial-gradient(circle closest-side,  rgba(58, 104, 68, 0.90) 94%, #0000) right/calc(200% - 1em) 100%;
            animation: l24 1s infinite alternate linear;
        }

        .loader::before {
            content: "Loading...";
            line-height: 1em;
            color: #0000;
            background: inherit;
            background-image: radial-gradient(circle closest-side, #fff 94%, #000);
            -webkit-background-clip: text;
            background-clip: text;
        }

        @keyframes l24 {
            100% {
                background-position: left;
            }
        }
    </style>