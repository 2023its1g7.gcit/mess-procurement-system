@extends('layouts.auth-master')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <form method="post" action="{{ route('login.perform') }}">
        
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <img class="mb-4" src="{!! url('images/logogcit-removebg-preview.png') !!}" alt="" width="100" height="105">
        
        <h1 class="h3 mb-3 fw-normal">ADMIN LOGIN</h1>

        @include('layouts.partials.messages')


        <div class="input-group mb-3 ">
        <input style="height:50px" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username" required="required" autofocus>
           
            @if ($errors->has('username'))
                <span class="text-danger text-left">{{ $errors->first('username') }}</span>
            @endif
        </div>
        
        

        <div class="input-group mb-3">
            <input style=" border-right:0px solid white;" type="password" class="form-control" name="password" id="password" value="{{ old('password') }}" placeholder="Password" required="required">
            <span class="input-group-text" 
            style="background-color:white; border-left:0px solid white;height:50px">
                <i id="togglePassword" class="fa fa-eye" aria-hidden="true"></i>
            </span>
            @if ($errors->has('password'))
                <span class="text-danger text-left">{{ $errors->first('password') }}</span>
            @endif
        </div>

        <div class="form-group row mb-3">
            <div class="col-md-6">
                <!-- Input field goes here -->
            </div>
            <div class="col-md-6">
                <label for="remember" class="text-white">
                    <a  href="{{ route('ForgetPasswordGet') }}" style="color:white; text-decoration:underline;margin-left:16px;font-family: 'Helvetica', sans-serif;">Forgot Password</a>
                </label>
            </div>
        </div>


        <button class="w-100 btn btn-lg btn-primary custom-btn-color" type="submit">Login</button>
    
        <div class="semi-circle"></div>
        
    </form>
    

    <script>
    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password');

    togglePassword.addEventListener('click', function () {
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        this.classList.toggle('fa-eye');
        this.classList.toggle('fa-eye-slash');
    });
</script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <style>
        .custom-btn-color {
    background-color: #417A4E;
    transition: background-color 0.3s, color 0.3s; /* Add a smooth transition effect */
    border: 2px solid transparent; 
    font-family: 'Roboto', sans-serif;
}

.custom-btn-color:hover {
    background-color: white; /* Change background color to white on hover */
    color:#417A4E; /* Change font color to black on hover */

    border-color:#417A4E;
    font-family: 'Roboto', sans-serif;
    
}
.custom-btn-color:focus {

    border-color: #417A4E; /* Change the border color to green on focus */
   
}
h1 {
    color:white; /* Set the font color to white */
    font-family: 'Helvetica', sans-serif; /* Set the font family to Helvetica */
}
form {
    position: relative;
    z-index: 1;
    padding: 20px;
    background: rgba(255, 255, 255, 0.2); /* Transparent white background for the frosted glass effect */
    backdrop-filter: blur(10px); /* Apply a blur filter to simulate frosted glass */
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
    border-radius:8px;
}
.form-control:focus {
    border-color:#417A4E; /* Set the border color to green when in focus */
    box-shadow: 0 0 0 0.2rem rgba(0, 128, 0, 0.25); /* Add a green shadow when in focus */
}
    </style>
@endsection

