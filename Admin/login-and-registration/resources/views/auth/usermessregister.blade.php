@extends('layouts.auth-master')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

<style>
  button:hover {
    color: green; /* Change to the desired text color on hover */
    border: 2px solid green; /* Change to the desired border color on hover */
  }

  button a {
    color: #417A4E; /* Set the initial text color */
    text-decoration: none;
  }
</style>
<div style="position: absolute; top:30px; left:30px;">
    
    <a href="/users" style="color: white; text-decoration: none;">
    <i class="fas fa-arrow-left"></i>  Back
    </a>
    
    </div>   

@section('content')
    <style>
      
    
    .custom-button {
        background-color:#4B8B5A;
        color:white;
        border: 1px solid #4B8B5A;
        padding: 10px 20px;
        text-decoration: none;
        display: inline-block;
        transition: all 0.3s ease-in-out;
        cursor: pointer;
    }

    .custom-button:hover {
        background-color: white;
        color: #4B8B5A;
        border-color:#4B8B5A;
    }

    </style>

<style>
      
    
      .custom-button1 {
          background-color:#4B8B5A;
          color:white;
          border: 1px solid #4B8B5A;
          text-decoration: none;
          display: inline-block;
          transition: all 0.3s ease-in-out;
          cursor: pointer;
      }
  
      .custom-button1:hover {
          background-color: white;
          color: #4B8B5A;
          border-color:#4B8B5A;
      }
  
      </style>
   <style>
    form {
    position: relative;
    z-index: 1;
    padding: 20px;
    background: rgba(255, 255, 255, 0.2); /* Transparent white background for the frosted glass effect */
    backdrop-filter: blur(10px); /* Apply a blur filter to simulate frosted glass */
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
    border-radius:8px;
}
.form-control:focus {
    border-color:#417A4E; /* Set the border color to green when in focus */
    box-shadow: 0 0 0 0.2rem rgba(0, 128, 0, 0.25); /* Add a green shadow when in focus */
}
   </style>

    <form method="post" action="{{ route('usermessregister.perform') }}"  onsubmit="return validateForm()">

        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <h1 class="h3 mb-3 fw-normal" style="color:white">Add User</h1>


        <div class="form-group row">
                            
                             
        <div class="col-md-12  mb-3 ">
            <!-- <input type="text"  placeholder="Name" class="form-control" name="name" required autofocus> -->
            <input type="text" placeholder="Name" class="form-control" name="name" id="nameInput" required autofocus>
            
            <span id="nameError" style="color: red;"></span>
            
            @if ($errors->has('name'))
                <span class="text-danger">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>

        <div class="form-group row">
                            
                             
            <div class="col-md-12  mb-3 ">
                
                <input type="text" placeholder="Email" class="form-control" name="email" id="customEmail" required autofocus>
                <span id="emailError" style="color: red;"></span>
                
                @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
            </div>
        </div>

        <script>
            function validateForm() {
                // Validate email
                var emailInput = document.getElementById('customEmail');
                var emailError = document.getElementById('emailError');
                var emailPattern = /^\d{8}\.gcit@rub\.edu\.bt$/;

                // Validate name
                var nameInput = document.getElementById('nameInput');
                var nameError = document.getElementById('nameError');
                var namePattern = /^[A-Za-z ]+$/;

                // Check email
                if (emailPattern.test(emailInput.value)) {
                    emailError.textContent = ''; // Clear any previous error message
                } else {
                    emailError.textContent = 'Invalid email format';
                    return false; // Prevent the form from being submitted
                }

                // Check name
                if (namePattern.test(nameInput.value)) {
                    nameError.textContent = ''; // Clear any previous error message
                } else {
                    nameError.textContent = 'Please enter only letters';
                    return false; // Prevent the form from being submitted
                }

                // Continue with form submission or other actions
                return true; // Allow the form to be submitted
            }
        </script>
        
        <div class="form-group row">
                            
                             
        <div class="col-md-12  mb-3 ">
            <!-- <input type="number"   placeholder="Student_id" class="form-control" name="username" maxlength="8" required autofocus> -->
            <input type="number" placeholder="Student_id" class="form-control" name="username" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="8" required autofocus>

            @if ($errors->has('username'))
                <span class="text-danger">{{ $errors->first('username') }}</span>
            @endif
        </div>
         </div>

        
        <div class="input-group mb-3 col-md-12">
        <input  style="border-right:0px solid white;" type="password" placeholder="Password" id="password" class="form-control" name="password" required autofocus>
        <span class="input-group-text" style="background-color:white;border-radius:0px 6px 6px 0px; border-left:0px solid white; height:38px">
          <i id="togglePassword" class="fa fa-eye" aria-hidden="true"></i>
        </span>
        @if ($errors->has('password'))
          <span class="text-danger">{{ $errors->first('password') }}</span>
        @endif
      </div>

      <div class="input-group mb-3 col-md-12">
        <input style="border-right:0px solid white;"  type="password" placeholder="Confirm Password" id="password-confirm" class="form-control" name="password_confirmation" required autofocus>
        <span class="input-group-text" style="background-color:white;border-left:0px solid white;height:38px;border-radius:0px 6px 6px 0px">
          <i id="togglecon" class="fa fa-eye" aria-hidden="true"></i>
        </span>
        @if ($errors->has('password_confirmation'))
          <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
        @endif
      </div>

        <button class="w-100 btn btn-sm custom-button" type="submit">Add</button>
        
    
    </form>


 <!-- Include Bootstrap JavaScript (jQuery and Popper.js) -->
 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <style>
        /* Custom button styles */
        .custom-button {
            background-color: #4B8B5A;
            color: white;
            border: 1px solid transparent;
            padding: 10px 20px;
            text-decoration: none;
            display: inline-block;
            transition: all 0.3s ease-in-out;
            cursor: pointer;
        }

        .custom-button:hover {
            background-color: white;
            color: #4B8B5A;
            border-color: #4B8B5A;
        }

        /* Center the form */
        .container {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }
    </style>

<script>
    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password');

    togglePassword.addEventListener('click', function () {
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        this.classList.toggle('fa-eye');
        this.classList.toggle('fa-eye-slash');
    });
  </script>

  <script>
    const togglePassword1 = document.querySelector('#togglecon');
    const passwordconf = document.querySelector('#password-confirm');

    togglePassword1.addEventListener('click', function () {
        const type = passwordconf.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordconf.setAttribute('type', type); // Change 'password' to 'passwordconf'
        this.classList.toggle('fa-eye');
        this.classList.toggle('fa-eye-slash');
    });
  </script>

@endsection