<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\MessuserController;

use App\Http\Controllers\IssueController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SpoiledController;
use App\Http\Controllers\BalanceController;
use App\Http\Controllers\BalanceHistoryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SearchPurchaseController;
use App\Http\Controllers\SearchSpoiledItemController;
use App\Http\Middleware\ForceLogout;

Route::group(['namespace' => 'App\Http\Controllers'], function()
{   

    Route::get('/loading.load', 'LoadingController@show')->name('loading.load');
    Route::get('/loading1.load', 'LoadingController1@show')->name('loading1.load');
    Route::get('forget-password', [ForgotPasswordController::class, 'ForgetPassword'])->name('ForgetPasswordGet');
    Route::post('forget-password', [ForgotPasswordController::class, 'ForgetPasswordStore'])->name('ForgetPasswordPost');
    Route::get('reset-password/{token}', [ForgotPasswordController::class, 'ResetPassword'])->name('ResetPasswordGet');
    Route::post('reset-password', [ForgotPasswordController::class, 'ResetPasswordStore'])->name('ResetPasswordPost');
    
   

    Route::group(['middleware' => ['web']], function() {
 
        Route::get('/', 'LoginController@show')->name('login.show');
        Route::post('/', 'LoginController@login')->name('login.perform');

        Route::get('/logout', 'LogoutController@perform')->name('logout.perform');

    });

    Route::group(['middleware' => ['web']], function () {
        // Private routes

        Route::get('/usermessregister', 'UserMessRegisterController@show')->name('usermessregister.show');
        Route::post('/usermessregister', 'UserMessRegisterController@usermessregister')->name('usermessregister.perform');

        Route::get('/users', 'MessuserController@index')->name('users.index');
        Route::delete('/users/{user}', 'MessuserController@destroy')->name('users.destroy');

        Route::get('/product', [ProductController::class, 'index'])->name('product.index');
        Route::get('/addpurchased', [ProductController::class, 'create'])->name('product.create');
        Route::post('/product', [ProductController::class, 'store'])->name('product.store');
        Route::get('/product', [SearchPurchaseController::class, 'index'])->name('product.index');
    
        Route::get('/issuelist', [IssueController::class, 'index'])->name('issuelist');
        Route::get('/addissue', [IssueController::class, 'create'])->name('addissue');
        Route::post('/issuelist', [IssueController::class, 'store'])->name('issuelist');
        Route::get('/issuelist', [SearchController::class, 'index'])->name('issuelist');
        
        Route::post('/addissue', [CategoryController::class, 'store'])->name('storecategory');
      
        Route::get('/addissue', [CategoryController::class, 'index'])->name('addissue');
        Route::get('/addpurchased', [CategoryController::class, 'index1'])->name('product.create');
        Route::get('/spoillist', [CategoryController::class, 'index2'])->name('spoillist');
        
        Route::get('/spoillist', [SpoiledController::class, 'index'])->name('spoillist');
        Route::get('/addspoil', [SpoiledController::class, 'create'])->name('addspoil');
        Route::post('/spoillist', [SpoiledController::class, 'store'])->name('spoillist');
        Route::get('/spoillist', [SearchSpoiledItemController::class, 'index'])->name('spoillist');
    
        Route::get('/api/getUniqueUnits', [CategoryController::class, 'getUniqueUnits']);
        Route::get('/api/getUniqueItemNames', [CategoryController::class, 'getUniqueItemNames']);
        
        Route::get('/balancecheck', [BalanceController::class, 'showDashboard'])->name('balancecheck');
        Route::get('/balancehistory', [BalanceHistoryController::class, 'showDashboard'])->name('balancehistory');
        
        Route::get('/issuelist-{issue_id}', [IssueController::class, 'edit'])->name('editIssue');
        Route::put('/issuelist-{issue_id}', [IssueController::class, 'update'])->name('updateIssue');

        Route::get('/dashboard', [DashboardController::class, 'showDashboard'])->name('home.private');

    });

});

