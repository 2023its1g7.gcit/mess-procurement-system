<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    protected function redirectTo(Request $request): ?string
    {
        // Check if the user is authenticated
        if ($request->user() && !$request->expectsJson()) {
            // If authenticated, redirect to the home page or any other desired route
            return route('home.private');
        }

        // If not authenticated, redirect to the login page
        return $request->expectsJson() ? null : route('login.show');
    }
}
