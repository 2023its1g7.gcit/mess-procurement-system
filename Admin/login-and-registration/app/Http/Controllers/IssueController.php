<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Models\Issue;
use App\Models\Category;

class IssueController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $issues = Issue::orderBy('created_at', 'desc')->get();
        $groupedIssues = $issues->groupBy(function ($issue) {
            return $issue->created_at->format('d/m/y h:i:s A');
        });
        
        
        return view('issuelist', ['groupedIssues' => $groupedIssues]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('addissue');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'sl_no.*' => 'required|numeric',
            'item_name.*' => 'required',
            'quantity.*' => 'required|numeric',
            'unit.*' => 'required',
        ]);

        // Extract the "Submitter" and "Meal" fields
        $submitter = $request->input('submitter')[0];
        $meal = $request->input('meal')[0];

        // Process and insert the data into the database
        foreach ($request->input('sl_no') as $key => $slNo) {
            Issue::create([
                'sl_no' => $slNo,
                'item_name' => $request->input('item_name')[$key],
                'quantity' => $request->input('quantity')[$key],
                'unit' => $request->input('unit')[$key],
                'submitter' => $submitter, // Use the same submitter for all rows
                'meal' => $meal, // Use the same meal for all rows
            ]);
        }

        // Redirect to the product index page or return a response as needed
        return redirect(route('issuelist'));
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }


    // public function edit($issue_id)
    // {
    //     $issue = Issue::findOrFail($issue_id);
    //     return view('editIssue', compact('issue'));
    // }
    public function edit($issue_id)
    {
        $issue = Issue::findOrFail($issue_id);
        $categories = Category::all();

        return view('editIssue', compact('issue', 'categories'));
    }


    public function update(Request $request, $issue_id)
    {
        $issue = Issue::findOrFail($issue_id);

        $request->validate([
            'item_name.*' => 'required',
            'quantity.*' => 'required|numeric',
            'unit.*' => 'required',
        ]);

        // Assuming 'item_name', 'quantity', 'unit' are arrays in the request
        $issue->update($request->only('item_name', 'quantity', 'unit'));

        // return redirect()->back()->with('success', 'Issue updated successfully.');
        return redirect()->route('issuelist');
    }



    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
