<?php

namespace App\Http\Controllers;

// use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\Product;
// use Illuminate\Http\Response;
// use App\Models\Product;
// // use App\Models\Product;
// use Illuminate\View\View;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->get();
        $groupedProducts = $products->groupBy(function ($product) {
            return $product->created_at->format('d/m/y h:i:s A'); // Group by date
        });
        
        return view('products.index', ['groupedProducts' => $groupedProducts]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'sl_no.*' => 'required|numeric',
            'vendor.*' => 'required',
            'item_name.*' => 'required',
            'quantity.*' => 'required|numeric',
            'unit.*' => 'required',
            'rate.*' => 'required|numeric',
            'amount.*' => 'required|numeric',
            'cheque_no.*' => 'required',
            'amt.*' => 'required|numeric',
        ]);

        // Extract the "Submitter" and "Meal" fields
        $submitter = $request->input('submitter')[0];
        // $meal = $request->input('meal')[0];

        // Handle image upload if 'images' is present in the request
        $imagePath = null;
        if ($request->hasFile('images')) {
            $image = $request->file('images')[0]; // Take the first image
            // Generate a unique filename for the image
            $imageName = time() . '-' . $image->getClientOriginalName();
            // Move the uploaded image to the specified directory
            $image->move(public_path('uploads'), $imageName);
            // Set the image path
            $imagePath = 'uploads/' . $imageName;
        }

        // Process and insert the data into the database
        foreach ($request->input('sl_no') as $key => $slNo) {
            Product::create([
                'sl_no' => $slNo,
                'vendor' => $request->input('vendor')[$key],
                'quantity' => $request->input('quantity')[$key],
                'item_name' => $request->input('item_name')[$key],
                'unit' => $request->input('unit')[$key],
                'rate' => $request->input('rate')[$key],
                'amount' => $request->input('amount')[$key],
                'cheque_no' => $request->input('cheque_no')[$key],
                'amt' => $request->input('amt')[$key],
                'image_path' => $imagePath, // Use the same image for all rows
                'submitter' => $submitter, // Use the same submitter for all rows
                // 'meal' => $meal, // Use the same meal for all rows
            ]);
        }

        // Redirect to the product index page or return a response as needed
        return redirect(route('product.index'));
    }



    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    
}

