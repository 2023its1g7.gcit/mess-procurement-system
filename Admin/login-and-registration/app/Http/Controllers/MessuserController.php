<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Messuser;
use Illuminate\Support\Facades\DB;

class MessuserController extends Controller
{
    public function index()
    {
        $users = DB::table('messusers')->get(); // Use your table name here.

        return view('User', compact('users'));
    }
    public function destroy($id)
    {
        Messuser::find($id)->delete();

        // Redirect back or to another page after deletion
        return redirect()->route('users.index');
    }
}
