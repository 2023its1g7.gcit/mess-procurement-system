<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        
        return view('addissue', ['categories' => $categories]);
        
    }
    public function index1()
    {
        $categories = Category::all();
        
        return view('products.create', ['categories' => $categories]);
    }

    public function index2()
    {
        $categories = Category::all();
        
        return view('spoileditem', ['categories' => $categories]);
    }

    


    public function store(Request $request)
    {
        $data = $request->validate([
            'item_name' => 'required',
            'unit' => 'required',
            'threshold' => 'required',
        ]);

        $data['item_name'] = ucwords(strtolower($data['item_name'])); // Convert to lowercase and then capitalize first letter
        $data['unit'] = strtolower($data['unit']); // Convert to all lowercase

        // Create a new category with the provided data
        $newCategory = Category::create($data);

        // Redirect back to the combined view of listing issues and the form for adding an issue
        return redirect(route('addissue'));
    }

    public function getUniqueUnits(Request $request)
    {
        // Fetch unique unit values from the 'categories' table
        $uniqueUnits = Category::select('unit')
            ->distinct()
            ->pluck('unit');


        // Return the unique unit values as JSON response
        return response()->json($uniqueUnits);
    }


    public function getUniqueItemNames(Request $request)
    {
        // Fetch unique item names from the 'categories' table
        $uniqueItemNames = Category::select('item_name')
            ->distinct()
            ->pluck('item_name');

            $uniqueItemNames = $uniqueItemNames->concat(['Onion',]);

        // Return the unique item names as a JSON response
        return response()->json($uniqueItemNames);
    }

}
