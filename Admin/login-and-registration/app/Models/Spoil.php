<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spoil extends Model
{
    protected $table ='spoiled_list';
    protected $primaryKey = 'spoil_id';
    protected $fillable = ['sl_no', 'item_name', 'quantity', 'unit', 'submitter',];

    use HasFactory;
}
