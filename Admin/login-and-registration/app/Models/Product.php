<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table ='purchased_list';
    protected $primaryKey ='id';
    protected $fillable = ['sl_no', 'vendor', 'quantity','item_name', 'unit', 'rate', 'amount', 'cheque_no', 'amt', 'image_path','submitter'];

    use HasFactory;
}
