<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    protected $table ='issue';
    protected $primaryKey = 'issue_id';
    protected $fillable = ['sl_no', 'item_name', 'quantity', 'unit', 'submitter', 'meal',];

    use HasFactory;
}
